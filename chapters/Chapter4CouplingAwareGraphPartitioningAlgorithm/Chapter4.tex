%--------------------------------------------------------------------------------------------------
%
\chapter{Graph partitioning algorithm for interface-coupled multiphysics problems}
\section{Introduction and motivation}
In the previous chapter, the generic parsing algorithm was moved a step further towards a fully functioning algorithm capable of setting up interface-coupled multiphysics and single domain multiphysics problems by incorporating interface functionalities and features that link OpenCMISS-Iron derived type objects with identical Ids. The correct implementation of the features was demonstrated by setting up an interface-coupled (or multidomain) Laplace and multiphysics poroelasticity problem. After implementing the aforementioned features the next task will be to develop and implement a graph partitioning algorithm in OpenCMISS-Iron that partitions multiple phases while respecting the interface between them. This essentially implies that that mesh partitioning is carried out such that the information transfer (or communication volume) among processors across the interface and within the coupled mesh domains is minimized as well as vertex weights are equally distributed among the processors. This decrease in the intercommunication volume among processors in a generic interface-coupled multiphysics problem is achieved by imposing similar subdomain Ids on vertices in graphs existing on either side of the interface. In other words, a mesh partitioning algorithm needs to be implemented in OpenCMISS-Iron that is ``aware of'' the coupling between meshes with a common boundary. Implementing this task is a vital step towards developing the generic parsing algorithm for fluid-solid interaction problems. Hence this chapter proposes an algorithm to implement such ``coupling aware'' graph partitioning algorithm using ParMETIS. The remaining chapter is structured as follows: Section $4.2$ deals with the start of the art mesh partitioning capabilities of OpenCMISS-Iron and ParMETIS in the context of interface-coupled multiphysics problems. Section $4.3$ presents the formal statement of the coupling aware graph partitioning problem and the various elements used to implement the scheme behind the proposed problem. Section $4.5$ summarizes the chapter and draws necessary conclusions.
\section{State of the art capabilities of OpenCMISS-Iron for graph partitioning in interface-coupled multiphysics simulations }
As stated earlier in section 2.4, the state of the art mesh partitioning algorithm of OpenCMISS-Iron can only execute an element-wise partitioning using \textit{ParMETIS\_V3\_PartMeshKway} routine. This partitioning cannot be used to impose a ``coupling aware'' mesh partitioning as \textit{ParMETIS\_V3\_PartMeshKway} does not provide an option to assign edge weights, which is one of the features that ensure same subdomain Ids on either side of the interface. This point will be discussed in depth in later sections. Moreover, OpenCMISS-Iron partitions interface-coupled multiphysics problems in a naive sense such that mesh corresponding to each phase is partitioned independently. This naive partitioning may result in assigning dissimilar subdomain Ids to interface vertices of each mesh domain. This will result in an increase in the interprocessor communication cost, which would result in an increase of the overall computational cost of solving the problem. An illustration of the naive approach and coupling aware approach has been shown previously in figure 2.5.
\newline
Moreover, the state of the art algorithm of ParMETIS does not provide any subroutine that implements a fixed vertex partitioning i.e. a graph partitioning respecting a prescribed boundary condition. This feature is an integral part of developing a ``coupling aware partitioning'', as will be explained in the later sections.
\newline
With the aforementioned limitations of the state of the art algorithm of OpenCMISS-Iron and ParMETIS, there is a need to implement the following features in OpenCMISS-Iron.
\begin{enumerate}
\item \textit{ParMETIS\_V3\_PartKway} routine from ParMETIS library where users can assign weights to both vertices and edges.
\newline
\item Develop an algorithm capable of executing ``coupled aware'' graph partitioning of multiple phases with a common interface in order to minimise interprocessor communication cost across the interface. 
\end{enumerate}
\section{Algorithm}
The basic terminologies such as graph, edgecut, vertex weight, edgecut cost and mesh partitioning used in this section have already been introduced and discussed in detail in chapter $2$. This section presents a formal statement for a
``coupling aware'' graph partitioning problem and proposes multiple algorithms to solve the presented problem.
\subsection{Coupling aware graph partitioning problem}
In problems involving multiple graphs with a common boundary, there is a need to partition the graph while explicitly taking into account the coupling between the graphs. To solve this problem a scheme is proposed and implemented on models with three graphs representing coupled phase \textit{G}, coupled phase \textit{B} and the interface \textit{I}. From here onward any quantity with subscript \textit{A}, \textit{B} and \textit{I} will represent quantities belonging to coupled phase \textit{A}, \textit{B} and the interface respectively. 
\newline
\newline
More formally, consider graphs representing phase \textit{A}, \textit{B} and \textit{I} represented as $G_A=(V_A,E_A)$, $G_B=(V_B,E_B)$ and $G_I=(V_I,E_I)$ respectively. The entire coupled model is represented with global mesh $G_{ABI}=(V_{ABI}, E_{ABI})$ where $V_{ABI}= V_A \cup V_B \cup V_I$ and $E_{ABI}=E_A \cup E_B \cup E_I \cup I_{AI} \cup I_{BI}$. $I_{AI}$ and $I_{BI}$ are the intermediate edges (or simply interedges) representing a binary relation between the interface mesh $G_I$ and coupled mesh graphs $G_i$, where $i=A$, $B$. $I_{AI}$ and $I_{BI}$ are termed as interedges as they connect the vertices of different graphs. In physical terms, these interedges represent the communication cost between vertices of interface subgraph $G_A^I$, $G_B^I$ respectively. $G_A^I$, $G_B^I$ are nothing but graphs restricted to interface boundary of each coupled mesh.
\newline
\newline
In this context, a \textit{K}-way partitioning of global mesh $G_{ABI}$ represented by $P_{ABI}$ is needed to be found such that weights assigned to set of vertices $V_{ABI}$ are equally distributed among processors and number of edgecuts and inter-communication volume between vertices of the graphs $G_{ABI}$ is minimised. Figure $4.1$ illustrates aforenamed notations for a simple 2D model.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch4figure7.png}
\captionsetup{labelformat=empty}
\caption{Figure 4.1 Elements of the global mesh $G_{ABI}$.}
\end{figure}
\subsection{Graph operators}
In order to facilitate the introduction of the proposed algorithm, some graph operations will be explained here, namely: partition, projection, restriction, weight imposition and fixed vertex partition operators. These operators are building blocks of the proposed algorithm. A detailed description of these operations is as follow.
\begin{enumerate}
\item Partition operator
\newline
Partition operator, when applied to the graph \textit{G}, returns a \textit{K}-way balanced partitioning. Symbolically, it is denoted as $Part(G,K)\to P$. In the algorithm, the partition operation is achieved using \textit{ParMETIS\_V3\_PartKWay} subroutine. As explained before in chapter $2$, this subroutine, in addition to other parameters, takes graph information such as \textit{adjncy} matrix as the input argument and executes vertex-wise graph decomposition. The calling sequence of this subroutine is described in detail in appendix $1$. Figure $4.2$ illustrates a $4$-way partition of graph \textit{G}.
\begin{figure}[H]
\centering
\includegraphics[width=0.93\textwidth]{ch4figure1.png}
\captionsetup{labelformat=empty}
\caption{Figure 4.2 A \textit{4-way} partition of the graph \textit{G} i.e. \textit{Part(G,4)}.}
\end{figure}
\item Restriction operator
\newline
Restriction operator when applied on coupled mesh graph $G_A(V_A,E_A)$ and interface graph $G_I(V_1,E_1)$ gives subgraphs $G_A^I(V_A^1,E_A^I)\subset G_A$ such that the subgraphs $G_A^I$ physically represent the common boundary of graphs $G_A$ and $G_I$. Symbolically, its denoted as: $Rest(G_A,G_I)\to G_A^I$. Restriction operation is achieved by building interedges $I_{AI}$ between vertices $V_A^I$ and $V_I$ as demonstrated in the following stated projection operator. 
\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{ch4figure2.png}
\captionsetup{labelformat=empty}
\caption{Figure 4.3 Restriction operator applied on the coupled graph $G_A$ and the interface graph $G_I$ i.e. $Rest(G_A,G_I)\to G_A^I$.}
\end{figure}
\item Projection operator
\newline
\newline
Projection operator when applied on Graph $G_A(V_A,E_A)$ and $G_I(V_I,E_I)$ builds one-to-one or one to many relationships between the set of vertices of the coupled mesh graph restricted to the interface front $V_A^I$ and the interface graph $V_I$. Symbolically, it is denoted as: $Proj(v_A^I)\to v_I$ where $v_A^I \in V_A^I$, $v_I \in V_I$.
\newline
\newline
\textit{Proj} operator builds one-to-one relationship if and only if for every $v_A^I \in V_A^I$ there is \textit{atmost} one $v_I \in V_I$.
\newline
\newline
\textit{Proj} operator builds one-to-many relationships if and only if for every $v_A^I \in V_A^I$ there is at atleast one $v_I \in V_I$.
\newline
\newline
In physical terms, the \textit{Proj} operator builds a binary relationship between vertices of graphs $G_A^I(V_A^I,E_A^I)$ and $G_I(V_I,E_I)$. The binary relationship is based on their spatial proximity. Inside the algorithm, the following scheme is adopted to build the interedges.
\begin{enumerate}
\item In the first step, a subgraph $G_A^b (V_A^b , E_A^b)$ representing the boundary of coupled mesh $G_A(V_A , E_A)$ such that $G_A^b \subset G_A$ is isolated and its respective vertices $V_A^b$ are stored in an array.
\item In the second step, length of the longest edge $e_I^{max}$ of the interface mesh graph $G_I (V_I , E_I)$ i.e. $e_I^{max} \in E_I$ is calculated.
\item In the third step, the $L2$-norm of each vertex belonging to set of vertices $V_A^b$ is calculated with respect to the vertices $V_I$. As a result, an array is built that store vertices $V'^b_A$ such that $L2$-norm of vertex $v'^b_A \in V'^b_A$ with respect to the interface mesh vertices $V_I$ is less than the longest edge of the interface graph $e_I^{max}$. In terms of algorithm, this step can be presented as follows.
\newline
\newline
\fbox{\begin{minipage}{35em}
\-\hspace{0.5cm} \textit{DO} \textit{CoupledMeshVertexIndex} = \textit{$1$, SIZE($V_A^b$)} 
\newline
\newline
\-\hspace{1cm} $v_A^b=V_A^b$ \textit{(CoupledMeshVertexIndex)}
\newline
\newline
\-\hspace{1cm} \textit{DO} \textit{InterfaceMeshVertexIndex} = \textit{$1$, SIZE($V_I$)} 
\newline
\newline
\-\hspace{1.5cm} $v_I = V_I$ \textit{(InterfaceMeshVertexIdx)}
\newline
\newline
\-\hspace{1.5cm} \textit{IF}( $L2$-norm$(v_I - v_A^b ) < NORM2(e_I^{max}))$\textit{ THEN}
\newline
\newline
\-\hspace{2.0cm} $V'^b_A$(\textit{InterfaceMeshVertexIdx})$=v_A^b$
\newline
\newline
\-\hspace{2.0cm} $V'_I$(\textit{InterfaceMeshVertexIdx})$=v_I$
\newline
\newline
\-\hspace{2.0cm} \textit{EXIT}
\newline
\newline
\-\hspace{1.5cm} \textit{END IF}
\newline
\newline
\-\hspace{1.0cm} \textit{END DO}
\newline
\newline
\-\hspace{0.5cm} \textit{END DO}
\end{minipage}}
\newline
\newline
Therefore this step essentially build a one-to-one relation between vertices $v'^b_A \subset V'^b_A$ and $v'_I \subset V'_I$ (in case of conforming meshes) or possibly one-to-many relations between $v'_I \subset V'_I$ and $v'^b_A \subset V'^b_A$ (in case of non-conforming meshes).
\newline
\item In the last step vertices $v'^b_A \subset V'^b_A$ are further filtered out by putting them through a sanity check where unit normal vectors $\mathbf{n_A}$ are calculated on each vertex $v'^b_A \subset V'^b_A$ and compared with the unit normal vectors $\mathbf{n_I}$ on their corresponding interface graph vertices $v'_I \subset V'_I$ such that if the angular difference between that $\mathbf{n_I}$ and $\mathbf{n_A}$ is less than $5^{\circ}$ then there exists a binary relationship between $v'^B_A$ and its corresponding interface mesh vertex $v'_I$. In algorithmic terms, this step can be presented as.
\newline
\newline
\fbox{\begin{minipage}{35em}
\-\hspace{0.5cm} \textit{DO} \textit{VertexIdx} =$1$, \textit{SIZE} ($V'^b_A$)
\newline
\newline
\-\hspace{1cm} $v'^b_A=V'^b_A$(\textit{VertexIdx})
\newline
\newline
\-\hspace{1cm} $v_I = V_I$ (\textit{VertexIdx})
\newline
\newline
\-\hspace{1cm} Calculate $\mathbf{n_A}$ on $v_A^b$
\newline
\newline
\-\hspace{1cm} Calculate $\mathbf{n_I}$ on $v_I$
\newline
\newline
\-\hspace{1cm} IF $\alpha = \cos^{-1} \left ( \frac{\mathbf{n_A}.\mathbf{n_I}}{|\mathbf{n_A}|.|\mathbf{n_I}|} \right ) < 5^{\circ}$ THEN 
\newline
\newline
\-\hspace{1.2cm} There exist binary relation between $v'^b_A$ and $v_I$. i.e. $v'^b_A \to v_I$
\newline
\newline
\-\hspace{1cm} \textit{END IF}
\newline
\newline
\-\hspace{0.5cm} \textit{END DO}
\end{minipage}} 
\newline
\newline
This step makes it essential for $v'^b_A \subset V'^b_A$ and $v_I \subset V_I$ to exist on the same boundary. Therefore if $v'^b_A$ and $v_I$ exist on the common boundary of couple mesh graph $G_A$ and $G_I$ , then one can imply that $v'^b_A= v_A^I \subset V_A^I$. 
\end{enumerate}
\item Set Weight operator
\newline
Set weight operator when applied to graph \textit{G}, imposes vertex and edge weight on boundary of the graph \textit{G}. Symbolically, it is denoted as \textit{SetWgt}$(G, W^v, W^e) \to G (V,E,W^e,W^v)$.
\newline
\item Fix Partition operator
\newline
Fix partition operator executes graph partitioning while respecting a prescribed boundary condition. The boundary condition is defined in terms of subdomain Ids prescribed to $G_A^I(V_A^I,E_A^I) \subset G_A$. Symbolically, \textit{FixPart} $(G_A, P'(G_A^I)) \to P_A$ such that the new partitioning $P_A$ respects the boundary condition.
This operation is achieved in the proposed algorithm using the following proposed schemes. 
\newline
\begin{enumerate} [label=(\roman*)]
\item Scheme A
\newline
In this scheme, a vertex fixed partition is achieved by meeting the following guidelines.
\begin{itemize}
\item 	A very high edge weight $w(e_A^I)$ (where $e_A^I \in E_A^I$) is prescribed on the edge whose end vertices are supposed to belong to the same subdomain (or processor Id). This step makes sure that \textit{ParMETIS} does not partition the graph about such edges.
\newline
\item 	An edge weight of zero $w(e_A^I)$ (where $e_A^I \in E_A^I$) is prescribed on the edge whose end vertices are supposed to belong to the different subdomains (or processor Ids). This step forces \textit{ParMETIS} to divide the graph across such edges.
\newline
\item A multi-constraint partitioning is used for vertex weights where two vertex weights are defined per vertex $v_A \in V_A$ i.e. $w^{1}(v_A)$ and $w^{2}(v_A)$. The first set of weights $w^1(v_A)$ corresponds to the actual computational load associated with the vertex $v_A \in V_A$, whereas the second set of vertex load $w^{2}(v_A)$ represents a virtual weight used to impose fixed vertex partitioning. More specifically, for vertices belonging to $G_A^I(V_A^I,E_A^I)$ a ``very large integer value'' is distributed as vertex weight among vertices belonging to each subdomain $V^I_{A,k}$ (where $V^I_{A,k}$ represents vertices belonging to subdomain \textit{k} such that $V^I_{A,k} \subset V^I_A$). Now the final partition will be carried out such that both sets of constraints $w^1(v_A^I)$ and $w^{2}(v_A^I)$ are equalised in each subdomain.
\end{itemize}
\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{ch4figure4.png}
\captionsetup{labelformat=empty}
\caption{Figure 4.4 Graph G with prescribed fixed vertex partition P(G).}
\end{figure}
Here it is quite intuitive that the aforementioned steps will ensure that the final partitioning respects the prescribed boundary condition. For the graph and fixed vertex partitioning illustrated in figure 4.4, following guidelines 1 and 2, very large edge weights $w^e(e_{5,10})$, $w^e(e_{15,20})$ and $w^e(e_{20,25})$ are imposed on edges $e_{5,10}$, $e_{15,20}$ and $e_{20,25}$ respectively. Whereas, a zero edge weight $w^e(e_{10,15})$ is imposed on the edge $e_{10,15}$ connecting vertex 10 and 15 respectively. Then following step $3$, a vertex weight $w^v_2 (v_5)$, $w^v_2 (v_{10})$ of $500$ units each is imposed on vertices belonging to the subdomain $1$ and vertex weight $w^v_2 (v_{15})$ , $w^v_2 (v_{20})$ and $w^v{(v_{25})}$ of $333$ units each is imposed on vertices belonging to the subdomain $2$. Such set of edge and vertex weights will enforce ParMETIS to obtain the partitioning that respects the prescribed partitioning in figure $4.4$. 
\item Scheme B
\newline
\newline
In scheme B, a vertex fixed partition is achieved as follows.
\begin{itemize}
\item 	Set of vertices $V_{A,k}^I$ belonging to each subdomain \textit{k} are collapsed to form one vertex $v_{A,k}^I$ per subdomain. As a result of this change in the graph topology, the \textit{adjncy} array is changed in a way that the adjacency of the collapsed vertex $v_{A,k}^I$ is union of the adjacency of set of vertices $V_{A,k}^I$.
\newline
\item 	A large vertex load $w_(v_{A,k}^I)$ is assigned to each collapsed vertex in order to make sure each collapsed vertex $v_{A,k}^I$ exists in a distinct subdomain.
\end{itemize}
Scheme B is illustrated in figure 4.5(a) where vertex $5$ and $10$ in figure are collapsed to vertex $5$ in 4.5(b) and vertices $15$, $20$ and $25$ are collapsed to form vertex $18$. Then following step $2$, a very large vertex load is applied to vertex $w^1(v_5)$ and $w^1(v_8)$ to make sure that as a result of graph partitioning each one of them is placed in a distinct subdomain.
\end{enumerate}
\end{enumerate}
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch4figure6.png}
\captionsetup{labelformat=empty}
\caption{Figure 4.5 Illustration of scheme B for imposing fixed vertex partitioning. }
\end{figure}
\subsection{Partition Algorithm}
The proposed algorithm solves the problem mentioned in section $4.3.1$ using \textit{Part, Rest, Proj, SetWeight} and 
 \textit{FixPart} operators. The proposed algorithm takes coupled mesh graphs $G_A$, $G_B$ and interface graphs $G_I$ as input and delivers graph partitioning $P_{AGI}$ as output i.e. \textit{Part} $(G_A(V_A ,E_A ), G_B (V_B ,E_B ), G_I(V_I ,E_I ), K) \to P_{AGI}$ such that the resulting partitioning make sure that vertices which are present in the same spatial location belonging to $v_I \in V_I$, $v_A^I \in V_A^I$ and $v_B^I \in V_B^I$ exists in the same subdomain. A full-length scheme of the proposed algorithm is as follows.
\begin{enumerate}
\item In first step, the interface graph undergoes \textit{K}-way partitioning i.e. \textit{Part}$(G_I,K) \to P_I$
\newline
\item In the second step, the Restriction operator is applied on $G_A$ to obtain the subgraph $G_A^I$ i.e. $Rest(G_A,G_I)\to G_A^I$
\newline
\item In this step interedges $\mathit{I_{AI}}$ are built between $G_A(V_A,E_A)$ and $G_I(V_I,E_I)$ i.e. $\mathit{Proj}(v_I) \to v^I_A.$
\newline
\item In this step, based on the established interedges $I_{AI}$, \textit{SetWeight} operator is used to impose vertex weights $W^v$ and edge weights $W^e$, on graph $G_A^I$ such that \textit{SetWeight}$(G_A, W^v, W^e) \to G_A (V,E, W^v, W^e)$.
\newline
\item Once the vertex weights $W^v$ and edge weights $W^e$ are imposed, the graph $G_A$ is partitioned using $\mathit{FixPart}$ operator such that $\mathit{FixPart}$ $(G_A(V,E,W^e,W^v), P'(G_A^I)) \to P _A$.
\newline
\item Repeat step $2-4$ for the second coupled mesh graph $G_B$ to obtain $P_B$.
\end{enumerate} 
After a successful execution of the proposed algorithm, a $K$-way partitioning of the global graph $G_{ABI}$ will be obtained i.e. $Part(G_A, G_B, G_I) \to P_{ABI}$. The resultant partitioning $P_{ABI}$, based on the spatial location, will have same subdomain Ids on interface boundary of the coupled mesh graphs $G_A$ and $G_B$ ensuring an optimum exchange of data between the subdomains and simultaneously a well distributed vertex load within each subdomain.
\section{Summary}
In this chapter, a partitioning scheme is presented that has a potential to meet all the goals of mesh partitioning of a generic interface-coupled multiphysics problem. The scheme has been described in terms of a number of graph operators that serve as building blocks for the mesh partitioning algorithm. Furthermore, each step of the partitioning scheme is illustrated using simple 2D schematics. Now in the next chapter, the ``coupling aware'' partitioning scheme will be implemented in the source code of OpenCMISS-Iron by introducing necessary subroutines and data structures in various modules of the source code. 

