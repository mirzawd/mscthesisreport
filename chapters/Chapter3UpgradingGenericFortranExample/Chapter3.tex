%--------------------------------------------------------------------------------------------------
%
\chapter{Generic parsing algorithm for interface-coupled multiphysics problems}
As mentioned in the previous chapter that state of the art parsing algorithm is only equipped to execute 
single physics and single domain problems. Therefore with an aim to solve a multiphysics and multidomain problems, the first goal is to add more functionalities in parsing algorithm, such that it is equipped to set up and solve generic multiphysics and interface-coupled multidomain problems. This chapter gives readers a comprehensive overview of the tasks performed to achieve this goal. The chapter is organized as follows. \\
\newline
Section $3.1$ sheds light on the rules and syntax that users are meant to follow in order to write the input file for the multidomain and multiphysics case studies. Section $3.2$ and $3.3$ documents the changes introduced in \textit{GenericFortranExample.f90} and \textit{OPENCMISS\_IRON.f90} files to link the newly introduced functionalities of the input file to the source code of OpenCMISS-Iron. In section $3.4-3.5$, a 2D multidomain Laplace and multiphysics poroelasticity problems are solved to demonstrate the working of newly added features of the parsing algorithm. Section $3.6$ summarizes the chapter and draws necessary conclusions.
\section{Upgrading Input.iron for multidomain and multiphysics problems.}
This section introduces readers to the functionalities that have been added to solve multidomain/multiphysics problems using input file user interface. The syntax of the newly added blocks are consistent with the state of the art format of the input file \cite{Mirza}. To get more insight into the state of the art syntax, the users of the input file are encouraged to read the documentation on state of the art parsing algorithm \cite{Mirza}. A set of new blocks are incorporated in the input file and parsing algorithm is updated to be able to read these blocks. Following sections detail the purpose, syntax and information that the users of the input file are supposed to provided within each block.
\subsection{INTERFACE block}
The purpose of interface block is to initializes derived type objects of type \textit{\textit{CMFE}\_\textit{InterfaceType}}. In this block, users are supposed to indicate the mesh Ids that are to be coupled. Using the interface block users can create multiple interfaces in a problem by introducing corresponding multiple blocks in the input file. For example, in a multidomain Laplace problems, the syntax for interface block would be as follows. \\

\fbox{\begin{minipage}{40em}
!\hspace{0.5cm} Interface block initialize object of data type CMFE\_Interface type \hspace{0.5cm} !
\newline
\newline
START\_INTERFACE
\newline
\newline
\hspace{0.5cm} INTERFACE\_ID
\newline
 INTERFACE
\newline
\newline
\hspace{0.5cm} MESH\_TO\_INTERFACE \hspace{3cm} !! IDs of the meshes to interface 
\newline
\hspace{0.5cm} DOMAIN\_1 , DOMAIN\_2 
\newline
\newline
\hspace{0.5cm} INTERFACE\_LABEL\_SET 
\newline 
\hspace{0.5cm} INTERFACE
\newline
\newline
END\_INTERFACE
\end{minipage}}
\newline
\newline
\subsection{INTERFACE\_MESH\_CONNECTIVITY block}
The purpose of this block is to initialize objects of derive type \textit{CMFE\_MeshConnectivityType} that store information about local \textit{Xi} coordinates of the nodes of the coupled mesh domains associated with the nodes of the interface mesh.
\newline
\newline
\fbox{\begin{minipage}{40em}
!\hspace{0.5cm} This block initialize object of data type CMFE\_InterfaceMeshConnectivity type \hspace{0.5cm} !
\newline
\newline
START\_INTERFACE\_MESH\_CONNECTIVITY
\newline
\newline
\hspace{0.5cm} INTERFACE\_MESH\_CONNECTIVITY\_ID
\newline
\hspace{0.5cm} INTERFACE
\newline
\newline
END\_INTERFACE\_MESH\_CONNECTIVITY
\end{minipage}}
\newline
\newline
\subsection{INTERFACE\_CONDITION block}
The purpose of this block is to initialize objects of derived type \textit{CMFE\_InterfaceConditionType} and let users prescribe the ``continuity of field'' interface condition from one of the following options.
\newline
\begin{enumerate}
\item FIELD\_CONTINUITY\_OPERATOR
\newline
This option is used for imposing continuity of a scalar field in a weak sense. The equation used to impose this condition is described in $3.1$. 
\begin{equation}
\int \delta \lambda(u_{1,gauss}-u_{2,gauss})dA=0
\end{equation}
\item FIELD\_NORMAL\_CONTINUITY\_OPERATOR
\newline
This option is used for imposing continuity of a vector field along normal of the interface in a weak sense. The equation used to impose this condition is described in $3.2$.
\begin{equation}
\int \delta \lambda (\mathbf{u_1}.\mathbf{n_1}-\mathbf{u_2}.\mathbf{n_2})dA=0
\end{equation}
\item FLS\_CONTACT\_PROBLEM
\newline
This option is used to impose the non-penetration condition for frictionless contact in a weak sense. The equation used to impose this condition is described in $3.3$.
\begin{equation}
\int \delta \lambda (\mathbf{x_1}.\mathbf{n_1}-\mathbf{x_2}.\mathbf{n_2})dA=0
\end{equation}
\item SOLID\_FLUID\_OPERATOR
\newline
This operator is used to impose continuity of velocities on the fluid-solid interface. The equation used to impose this condition is described in $3.4$. 
\begin{equation}
\int \delta \lambda (\mathbf{v_f}-\frac{d\mathbf{u_s}}{dt})dA=0
\end{equation}
\item INTERFACE\_CONDITION\_SOLID\_FLUID\_NORMAL\_OPERATOR
\newline
This operator is used to impose continuity of velocities along normal direction to fluid-solid interface. The equation used to impose this condition is described in $3.5$.
\begin{equation}
\int \delta \lambda (\mathbf{v_f}.\mathbf{n_f}-\frac{d\mathbf{u_s}}{dt}.\mathbf{n_s})dA=0
\end{equation}
\end{enumerate}
The ``continuity of derivative of field'' interface condition is imposed using one of the following options. 
\begin{itemize}
\item LAGRANGE\_MULTIPLIER\_METHOD option use Lagrange multiplier method to impose continuity of the derivative of the field across the interface. 
\item AUGMENTED\_LAGRANGE\_METHOD option uses the augmented Lagrange method to impose the continuity of the derivative of the field in a weak sense. 
\item PENALTY\_METHOD option use Penalty method to impose the continuity of the derivative of the field in a weak sense. 
\end{itemize}
The syntax for the interface condition block is as follows.
\newline
\newline
\newline
\fbox{\begin{minipage}{40em}
! INTERFACE\_CONDITION BLOCK initialize object of data type CMFE\_InterfaceCondition \hspace{0.5cm} !
\newline
\newline
START\_INTERFACE\_CONDITION
\newline
\newline
\hspace{0.5cm} INTERFACE\_CONDITION\_ID
\newline
\hspace{0.5cm} INTERFACE
\newline
\newline
\hspace{0.5cm} EQUATION\_SET\_TO\_ADD
\newline
\hspace{0.5cm} DOMAIN\_1,DOMAIN\_2
\newline
\newline
\hspace{0.5cm} INTERFACE\_CONDITION\_METHOD
\newline
\hspace{0.5cm} LAGRANGE\_MULTIPLIERS\_METHOD
\newline
\newline
\hspace{0.5cm} INTERFACE\_CONDITION\_OPERATOR
\newline
\hspace{0.5cm} FIELD\_CONTINUITY\_OPERATOR
\newline
\newline
END\_INTERFACE\_CONDITION
\end{minipage}}
\newline
\newline
\subsection{NODE\_SET block}
Node set block allows users to pass node Ids to the solver for prescribing the Dirichlet boundary condition. The syntax of the node set block and its interaction with the boundary condition block is follows.
\newline
\newline
\fbox{\begin{minipage}{40em}
START\_NODE\_SET
\newline
\newline
\hspace{0.5cm} NODE\_SET\_ID
\newline
\hspace{0.5cm} NODE\_SET\_1
\newline
\newline
\hspace{0.5cm} NODE\_SET \hspace{3cm} ! set of nodes to impose boundary conditions on
\newline
\hspace{0.5cm} 11,12,13
\newline
\newline
END\_NODE\_SET
\newline
\newline
START\_BOUNDARY\_CONDITIONS
\newline
\newline
BOUNDARY\_CONDITIONS\_ID
\newline
\hspace{0.5cm} DOMAIN\_1
\newline
\newline
\hspace{0.5cm} DIRICHLET \hspace{0.5cm} !!!! \hspace{0.5cm}[ TYPE , LOCATION , COMPONENTS , VALUE ]
\newline
\hspace{0.5cm} SURFACE, NODE\_SET\_1, 100, 1.0, BOUNDARY\_CONDITION\_FIXED, SET
\newline
\newline
END\_BOUNDARY\_CONDITIONS
\end{minipage}}
\newline
\newline
\section{Upgrading \textit{GenericFortranExample.f90} file for multi-domain/multi-physics problems}
In \textit{FortranExample.f90} file following subroutines have been introduced for updating \textit{GenerticFortraneExample.f90} file for multidomain/multiphysics problems. A detail on the calling sequence of the input and output arguments of these routines can be found in section $2.1$ of the appendix. 
\subsection{Subroutine \emph{MatchID( )}}
The \textit{MatchID( )} subroutine makes sure that derived type objects with the same Ids are linked together. For instance, in any multidomain test case, this routine links coordinate system objects with derived type \textit{CMFE\_CoordinateSystemType} to region objects with derived type \textit{CMFE\_RegionType}, with similar Ids. Internally this routine makes sure of a binary search algorithm to link objects belonging to identical coupled mesh domain. A flowchart of the internal working of this routine is shown in figure $3.1$.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch3figure1.png}
\captionsetup{labelformat=empty}
\caption{Figure 3.1 A flow chart representing working of subroutine \textit{MatchId( )}. The subroutine in this case will link \textit{CMFE\_CoordinateSystemType} and \textit{CMFE\_RegionType} objects belonging to the fluid domain.}
\end{figure}
\subsection{Interface subroutines}
For imposing continuity of field and fluxes on the interface of the coupled meshes following subroutines have been introduced.
\begin{itemize}
\item Subroutine \textit{InterfaceElementsGet( )} gives element Ids that exist on the interface of the coupled mesh.
\item Subroutine \textit{InterfaceNodesGet( )} gives node Ids that exist on the interface of the coupled mesh.
\item Subroutine \textit{GetInterfaceConnectivityMatrix( )} that generates data structure defining one to one binary relation between the interface elements of the coupled mesh and interface domains. 
\end{itemize}
The aforementioned subroutines work together in the interface block of the \textit{GenericFortranExample.f90} file to enforce the interface condition. For more information on these subroutines, users can refer to the section 2.2 of the appendix.
\section{Upgrading \textit{OPENCMISS\_IRON} module for multidomain/multiphysics problems}
Following subroutines have been added in \textit{OPENCMISS\_IRON} module to facilitate operations related to imposing interface conditions. A detail on the input and output data structures of these subroutines can be found in section $2.2$ of the appendix. 
\begin{itemize}
\item Subroutine \textit{CMFE\_Mesh\_ElementNodesGet( )} extracts node Ids belonging to a mesh element.
\item Subroutine \textit{CMFE\_Mesh\_NodeGetXi( )} extracts local \textit{Xi} coordinates of a node with respect to an element.
\item Subroutine \textit{CMFE\_DependentFieldNodeCheck( )} checks whether a node belongs to a component of the dependent field.
\end{itemize}
\section{Multidomain Laplace test case}
In this section, the aforementioned interface functionalities implemented in the parsing algorithm are demonstrated in a $2$D multi-domain Laplace problem. A stand-alone version of this test case can be found here \cite{Chris}. Here, this test case is executed and replicated using the upgraded parsing algorithm. The purpose of this task is to demonstrate the correct working of the updated generic parsing algorithm.
\subsection{Problem definition}
As mentioned before the problem at hand is a multidomain Laplace problem, governed by the following set of equations.
\begin{equation}
\Delta T_1=0 \hspace{4pt} in \hspace{4pt} \Omega_1
\end{equation}
\begin{equation}
T_1 \mid_{\Gamma_1^D}=\bar{T}_1 
\nonumber
\end{equation}
\begin{equation}
\Delta T_2=0 \hspace{4pt} in \hspace{4pt} \Omega_2
\end{equation}
\begin{equation}
T_2 \mid_{\Gamma_2^D}=\bar{T}_2 
\nonumber
\end{equation}
As the domains $\Omega_1$ and $\Omega_2$ are coupled, therefore in order to ensure the continuity of temperature and fluxes across the interface, the following two interface conditions are imposed.
\begin{enumerate}
\item Interface condition 1 ensures continuity of fluxes across the interface $\Gamma^c$ area in a weak sense using equation 3.8.
\begin{equation}
\int_{\Gamma^C} \delta T ( \lambda_1-\lambda_2)=0
\end{equation}
where $\delta T \in H^{-\frac{1}{2}}$ $\delta T_1 \in H^1$ and $\delta T_2 \in H^1$. Here $\lambda$ is Lagrange multiplier representing flux across the interface $\Gamma^c$ . $\delta T_1$ and $\delta T_2$ are the test functions against which the governing equations $3.6$ and equation $3.7$ are respectively solved using finite element analysis.
\item Interface condition $2$ enforces continuity of temperature field across the interface $\Gamma^c$ in a weak sense by imposing equation $3.9$.
\begin{equation}
\int_{\Gamma^c} \delta \lambda ( T_1-T_2)=0
\end{equation} 
Where $\delta \lambda \in H^{-\frac{1}{2}}$. A schematic representing the configuration of the case study is illustrated in figure $3.2$.
\end{enumerate}
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch3figure2.png}
\captionsetup{labelformat=empty}
\caption{Figure 3.2 A schematic representing the configuration of multidomain Laplace problem.}
\end{figure}
\subsection{Input file simulation parameter}
Following input file simulations parameters are used in this case study.
\begin{enumerate}
\item Geometric extents of coupled domain $1$ $\Omega_1$ spans over $x=[0,1]$ and $y=[0,1]$ , while for coupled domain$2$ $\Omega_2$ geometric extents spans over $x=[1,2]$ and $y=[0,1]$. For interface mesh, the geometric extents are $y=[0,1]$ and $x=1$. 
\item A generated mesh of size $5 \times 5$ consisting of linear Lagrange elements is used for $\Omega_1$ and $\Omega_2$. Whereas for the interface area $\Gamma^c$ a generated mesh consisting of $5$ linear Lagrange elements are used. 
\item In first configuration, Dirichlet boundary condition of $1$ unit is imposed on $\Gamma_1^D=\Omega_1 (0,y)$ and $0$ unit on $\Gamma_2^D=\Omega_2 (2,y)$. In the second configuration, a Dirichlet boundary condition of $1$ unit is imposed on $\Gamma_1^D=\Omega_1 (0,0)$ and $0$ unit on $\Gamma_2^D=\Omega_2 (2,1)$.
\item Direct linear solver is employed to solve the system of equations. 
\end{enumerate}
Input file simulation parameters for the second configuration of this case study are presented in detail in section 2.3 of the appendix.
\subsection{Results}
Results for first and second configuration are illustrated in figure $3.3$ and $3.4$ respectively. The colour contour represents the temperature field. It can be seen that the temperature field adheres to the Dirichlet boundary conditions and interface conditions (continuity of temperature) in both settings. Moreover, a linear variation of the temperature from $x=0$ to $x=2$ in both setting is observed due to the imposed boundary conditions. There results presented here exactly replicates the results of the original study \cite{Chris}.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch3figure3.png}
\captionsetup{labelformat=empty}
\caption{Figure 3.3 Results from the first configuration of the multidomain Laplace problem. The temperature linearly increases from left ($x=0$)  to the right face ($x=2$) of the domain.}
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch3figure4.png}
\captionsetup{labelformat=empty}
\caption{Figure 3.4 Results from the second configuration of the multidomain Laplace problem. The temperature in the domain linearly increases in the diagonal direction from $(0,0)$  to the $(2,1)$. }
\end{figure}
\section{Multiphysics poroelasticity test case}
In this section, a multiphysics poroelasticity problem is solved using the updated parsing algorithm. A standalone version of the test case can be found here \cite{Adam}. The purpose of this task is to verify whether the updated parsing algorithm can set up and solve single domain multiphysics problems. 

\subsection{Problem definition}
The problem at hand is a poroelasticity problem where fluid flow is coupled with the deformation of a porous media represented by a hexahedral domain. The deformation of the porous medium influences the fluid flow and vice versa. The assumptions involved in the study are as follows. 
\begin{itemize}
\item Fluid and solid phase have overlapping domains with distinct volume fractions. 
\item Physical properties are isotropic. 
\item The domain is subjected to gravity. 
\item The model is quasi-static i.e. rate of deformation is very small compared to the fluid flow. 
\item The fluid is laminar Newtonian i.e. inertial forces are very small compared to the viscous forces.
\item A change in fluid density can occur due to the fluid pressure i.e. $\rho=\rho(p_f)$. 
\end{itemize}
This test case is governed by the Darcy equation representing fluid flow in equation $3.10(a)$ and finite strain equation representing solid deformation in equation $3.10(b)$. Each partial differential equation (PDE) contains both fluid pressure and deformation of the porous media as state variables \cite{Showalter}. 
\begin{subequations}
\begin{eqnarray}
-(\lambda + 2\mu)\Delta \mathbf{u} + \alpha\nabla p_f = 0  
\end{eqnarray}
\begin{eqnarray}
-J(\nabla p_f + \rho \mathbf{g}) = 0 
\end{eqnarray}
\begin{eqnarray}
p_f \mid_{\Gamma^D}=\bar{p}_f; \mathbf{u} \mid_{\Gamma^D}=\bar{\mathbf{u}}
\end{eqnarray}
\end{subequations}
In equation $3.10(a)$, $\lambda$ is Lamma coefficient, $\mu$ is the shear modulus and $\alpha$ is the a physical parameter representing the fraction of pore surface in contact with the fluid. Whereas in equation $3.10(b)$, $\textit{J}$ represents permeability of laminar flow in the medium, $\mathit{p_f}$ represents the fluid pressure and $\rho$ represents fluid density. The Dirichlet boundary conditions are imposed in terms of fluid pressure and displacements. Moreover, the constitutive relations as mentioned in equation $3.11$ is the sum of effective stress of the of the elastic isotropic structure given by Hooke's law and effective pressure stress of the fluid on the structure. 
\begin{equation}
 \boldsymbol{\sigma} = p_f \mathbf{I} + \boldsymbol{\sigma^{\textit{hooks}}}
\end{equation}
\subsection{Input file simulation parameter}
Following input file simulations parameters are used in this case study.
\begin{enumerate}
\item The coupled domain $\Omega$ is represented as a hexahedron of volume $0.045\times 0.065\times 0.045$.
\item The coupled domain $\Omega$ is meshed using $2\times 3\times 2$ hexahedral elements with mixed interpolation basis. 
\item Dirichlet boundary conditions in terms of displacement and fluid pressure are imposed on $z=0$ plane. The nodes at $x=0$ are fixed, whereas the other value of the prescribed boundary conditions can be found here \cite{Adam}.
\item The coupled domain $\Omega$ is subjected to gravitational pull \textbf{g}.
\item Direct linear solver is employed to solve the system of equations. Whereas, Newton-Raphson solver is used for finding equilibrium values for each incremental value of the boundary conditions. 
\end{enumerate}
\subsection{Results}
The results of this test case is illustrated in figure $3.5$. The coupled domain $\Omega$ undergoes a three dimensional bending as a result of the displacement and pressure boundary condition. There results presented here replicates the results of the original study \cite{Adam}. 
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch3figure5.png}
\captionsetup{labelformat=empty}
\caption{Figure 3.5 Results of the multiphysics poroelasticity problem where a cantilevered domain undergoes a three dimensional bending as a result of the interaction of the fluid flow and porous medium resulting from the  prescribed displacement and pressure boundary conditions.}
\end{figure}
\section{Summary}
This chapter entails the first step towards upgrading the parsing algorithm towards solving an interface-coupled multiphysics(or multidomain) problem. The upgrades have been introduced in terms of the following.
\begin{enumerate}
\item Subroutine \textit{MATCH\_ID( )} that links derived type objects belonging to an identical domain. 
\item Subroutines that enforce the interaction between the coupled mesh domains using interface conditions.
\item Subroutines that are geared towards setting up and solving single domain multiphysics problems. 
\end{enumerate}
The correct working of the functionalities in generic parsing algorithm is verified by solving an interface-coupled multidomain Laplace and multiphysics poroelasticity study. The results from each test case replicated the results of the original multidomain Laplace study and multiphysics interface-coupled study present in \cite{Chris} and \cite{Adam} respectively. This verifies the new capabilities of the upgraded parsing algorithm for setting up multidomain and multiphysics problems.