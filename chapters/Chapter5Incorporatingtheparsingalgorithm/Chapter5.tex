%--------------------------------------------------------------------------------------------------
%
\chapter{Incorporating the parsing algorithm in OpenCMISS-Iron code base}
\section{Introduction}
In the previous chapter, the basic building blocks and the scheme behind the partitioning algorithm has been described in detail. Initially, the partitioning algorithm was developed and tested in a standalone format. Now in this chapter, the parsing algorithm will be incorporated in OpenCMISS-Iron by introducing subroutines and data structures in \textit{OPENCMISS\_IRON}, \textit{MESH\_ROUTINES} and \textit{TYPES} modules of OpenCMISS-Iron. The subroutines introduced in \textit{OPENCMISS\_IRON} module are called in the \textit{FortranExample.f90} file to activate the partitioning algorithm in an interface-coupled multiphysics study. Whereas, the partitioning algorithm itself is implemented in  \textit{MESH\_ROUTINE} module. In short, the partitioning algorithm is introduced in the following two steps.
\begin{enumerate}
\item Recall that the state of the art OpenCMISS-Iron source code contains only element-wise decomposition module. Therefore the first step is to introduce a trivial vertex based decomposition functionality in OpenCMISS-Iron. Here trivial vertex-based decomposition refers to the fact that coupled mesh $G_A$ and $G_B$ and interface graphs $G_I$ are partitioned independently without taking into account their respective interface fronts i.e. $G_A^I$ and $G_B^I$. 
\item In the second step, the partitioning algorithm is introduced in the OpenCMISS-Iron code base.
\end{enumerate}
In each step, the subroutines and data structures that are introduced in \textit{OPENCMISS\_IRON}, \textit{MESH\_ROUTINES} and \textit{TYPES} module are discussed in detail in this chapter. These changes are incorporated in the order illustrated in figure $5.1$. Moreover, the generic parsing algorithm is also updated such that users can activate the partitioning algorithm in an interface-coupled multiphysics study using input file user interface. The chapter is organized as follows: Section $5.2$ deals with the contribution made in the source code of OpenCMISS-Iron for introducing the vertex-based decomposition functionality in OpenCMISS-Iron. Section $5.3$ explains the subroutines introduced to OpenCMISS-Iron to integrate the partitioning algorithm. Section $5.4$ gives an overview of functionalities introduced in the parsing algorithm to execute the partitioning algorithm for any multiphysics interface study by providing required instructions via an input file. Section $5.5$ summarizes the chapter and draws necessary conclusions.
\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{ch5figure2.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.1 Workflow of the implementation of vertex-based decomposition and partitioning algorithm in OpenCMISS-Iron.}
\end{figure}
\section{Incorporating trivial vertex-based decomposition in OpenCMISS-Iron}
As mentioned in chapter $2$, the state of the art source files of OpenCMISS-Iron includes element-wise decomposition which cannot be used to impose the fixed vertex partitioning i.e. a partitioning that respects the boundary condition. Fixed vertex partitioning is an important component of the developed partitioning algorithm. Therefore as a first step, it is essential to implement vertex-wise decomposition in partitioning algorithm. With such feature implemented, users will now be able to solve any simulation study using vertex-wise decomposition, as well as use this feature in the partitioning algorithm. In order to implement vertex-wise decomposition, the three steps as illustrated in figure $5.1$ are followed as described below. 
\subsection{Derived data type members introduced in \textit{TYPES} module} 
In this subsection, the input and output data structures of the routines that are used to implement the vertex-based decomposition feature in OpenCMISS-Iron are explained in detail. Recall that, \textit{ParMETIS\_V3\_PartKway( )} subroutine of ParMETIS is used to calculate the vertex-based decomposition. Therefore in the first step, all the input arguments of \textit{ParMETIS\_V3\_PartKway( )} routine are declared as members of derived type \textit{DECOMPOSITION\_TYPE} in \textit{TYPES} module of OpenCMISS-Iron. The newly defined members of \textit{DECOMPOSITION\_TYPE} are:
\begin{enumerate}
\item \textit{DECOMPOSITION$\%$NODE\_BASED\_DECOMPOSITION} member of type \textit{BOOLEAN} and a default value of \textit{FALSE}, which is used to activate vertex-based decomposition by setting the member to \textit{TRUE}. 
\item \textit{DECOMPOSITION$\%$NUMBER\_OF\_CONSTRAINTS} member of type \textit{INTEGER}, which is used to define the number of constraints per vertex to balance.
\item \textit{DECOMPOSITION\%VERTEX\_WEIGHT} of type \textit{INTEGER} and size $DECOMPOSITION\%NUMBER\_OF\_CONSTRAINTS\times n$, where \textit{n} is the number of vertices local to each processor. 
\item \textit{DECOMPOSITION\%EDGE\_WEIGHT} of type \textit{INTEGER} and size $\mathit{2}$\textit{m}, where \textit{m} is the number of edges in a graph local to each processor.
\item \textit{DECOMPOSITION\%WEIGHT\_FLAG} member of type \textit{INTEGER} used to define whether a graph is weighted. This parameter can have $4$ possible values.
\begin{itemize}
\item $0$ which implies that \textit{DECOMPOSITION\%VERTEX\_WEIGHT} and \textit{DECOMPOSITION\%EDGE\_WEIGHT} are set to the default value of $1$. 
\item $1$ which implies that user can only define edge weight by storing values in \textit{DECOMPOSITION\%EDGE\_WEIGHT}. 
\item $2$ which implies that user can only define vertex weight by storing values in \textit{DECOMPOSITION\%VERTEX\_WEIGHT}. 
\item $3$ which implies that user can define weight on both vertices and edges by storing values in members \textit{DECOMPOSITION\%VERTEX\_WEIGHT} and \textit{DECOMPOSITION\%EDGE\_WEIGHT} respectively. 
\end{itemize}
\item \textit{DECOMPOSITION\%X\_ADJ}, \textit{DECOMPOSITION\%ADJNCY} members, which are arrays of type \textit{INTEGER} used to store vertex adjacency of a graph. 
\item \textit{DECOMPOSITION\%NUMBER\_OF\_PARTS} member which is a scalar of type \textit{INTEGER} used to define the number of subdomains that are desired. 
\item \textit{DECOMPOSITION\%UBVEC} member of type \textit{INTEGER} and size \textit{DECOMPOSITION\%NUMBER\_OF\_CONSTRAINTS} that is used to specify the imbalance tolerance for each vertex weight, with value equal to $1$ representing the perfect balance and a value equal to \textit{DECOMPOSITION\%NUMBER\_OF\_PARTS} representing the perfect imbalance. 
\item \textit{DECOMPOSITION\%TPWGTS} member used to specify the fraction of vertex weight that should be distributed to each subdomain for each balance constraint. 
\item \textit{DECOMPOSITION\%VTX\_DIST} is member of type \textit{INTEGER} used to define the vertices distribution among processors. 
\item \textit{DECOMPOSITION\%VERTEX\_DOMAINS} member of type \textit{INTEGER} which stores vertex domains assigned to the graph by the \textit{ParMETIS\_V3\_PartKway( )} subroutine.
\item \textit{DECOMPOSITION\%EDGE\_CUT} member of type \textit{INTEGER} which the stores number of edgecuts resulting from the graph partitioning. 
\end{enumerate}
In order to calculate \textit{DECOMPOSITION\%ADJNCY}, it is imperative to calculate the vertex Ids adjacent to each vertex of the graph. This piece of information is stored in the data structure \textit{SURROUNDING\_NODES(global\_vertex\_idx)}, where \textit{global\_node\_idx} is the global Id of the vertex for which the surrounding nodes are to be calculated. \textit{SURROUNDING\_NODES}(global\_vertex\_idx) data structure is introduced as a member of \textit{NODE\_TYPE} derived type.
\subsection{Subroutines introduced in \textit{MESH\_ROUTINES} module}
Each one of the above-stated members can be initialized as user defined input in the \textit{FortranExample.f90 file}. If the parameters are not defined by the user then default values are calculated using subroutines defined in \textit{MESH\_ROUTINE.f90}. The calling sequence of these subroutines is explained in the appendix section 3.1.1 and a brief detail along with their functionalities is explained as follows.
\begin{enumerate}
\item \textit{DECOMPOSITION\%NODE\_BASED\_DECOMPOSITION} with a default value of \textit{FALSE} initialized in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This parameter can be set to \textit{TRUE} by calling the subroutine \textit{DECOMPOSITION\_NODE\_BASED\_DECOMPOSITION\_SET( )}. 
\item \textit{DECOMPOSITION\%NUMBER\_OF\_CONSTRAINTS} is by default initialized to $1$ in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This parameter can be set to a user defined value using the subroutine \textit{DECOMPOSITION\_NUMBER\_OF\_CONSTRAINTS\_SET( )}.
\item \textit{DECOMPOSITION\%VERTEX\_WEIGHT} is by default set to $1$ in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This data structure can be set to the user defined values using the subroutine \textit{DECOMPOSITION\_VERTEX\_WEIGHT\_SET( )}. 
\item \textit{DECOMPOSITION\%EDGE\_WEIGHT} is by default set to $1$ in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This data structure can be set to the user defined values using subroutine \textit{DECOMPOSITION\_EDGE\_WEIGHT\_SET( )}.
\item \textit{DECOMPOSITION\%UBVEC} is by default value set to $1.0001$ in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This data structure can be set to the user defined value by calling the subroutine \textit{DECOMPOSITION\_UBVEC\_SET( )}.
\item \textit{DECOMPOSITION\%NUMBER\_OF\_PARTS} is by default set equal to the number of computational nodes used to execute the binary. 
\item \textit{DECOMPOSITION\%TPWGTS} is by default set to \( \frac{1.0}{DECOMPOSITION\%NUMBER\_OF\_PARTS} \) in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This data structure can be set to the user defined values using the subroutine \textit{DECOMPOSITION\_NODE\_BASED\_CALCULATE( )}.
\item The default value of array \textit{DECOMPOSITION\%VTX\_DIST} is defined such that each processor gets at maximum
\( \frac{TOTAL\_GLOBAL\_VERTICES}{DECOMPOSITION\%NUMBER\_OF\_PARTS} \) vertices. For instance, for a graph with $20$ vertices undergoing $3$-way partitioning will have \textit{DECOMPOSITION\%VTX\_DIST}$=[0, 7, 14, 20]$. The user defined values for \textit{DECOMPOSITION\%VTX\_DIST} can be set using routine \textit{DECOMPOSITION\_VTX\_DIST\_SET( )}. The default value of the array is calculated in the subroutine \textit{DECOMPOSITION\_VTX\_DIST\_DEFAULT\_SET( )}.
\item The default value of \textit{DECOMPOSITION\%WEIGHT\_FLAG} is set to $0$ in the subroutine \textit{DECOMPOSITION\_CREATE\_START( )}. This data structure can be set to the user defined value using subroutine \textit{DECOMPOSITION\_WEIGHT\_FLAG\_SET( )}.
\item One of the most important pieces of information is to calculate \textit{SURROUNDING\_NODES( )} data structure, which is later used in the \textit{GET\_ADJNCY\_AND\_XADJ( )} subroutine to calculate the graph adjacency. \textit{GET\_SURROUNDING\_NODES( )} routine is used to calculate this data structure. Inside the subroutine, a number of subroutines are called, each calculating \textit{SURROUNDING\_NODES( )} data structure for a different element type. These subroutines are as follows.
\begin{itemize}
\item \textit{GET\_SURROUNDING\_NODES\_LINEAR\_LINE\_ELEMENT( )} subroutine, which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for linear line elements.
\item \textit{GET\_SURROUNDING\_NODES\_QUADRATIC\_LINE\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for quadratic line elements.
\item \textit{GET\_SURROUNDING\_NODES\_CUBIC\_LINE\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for quadratic line elements.
\item \textit{GET\_SURROUNDING\_NODES\_LINEAR \_QUADRILATERAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for bi-linear quadrilateral elements.
\item \textit{GET\_SURROUNDING\_NODES\_QUADRATIC\_QUADRILATERAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for bi-quadratic quadrilateral elements.
\item \textit{GET\_SURROUNDING\_NODES\_CUBI\_QUADRILATERAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for bi-cubic quadrilateral elements.
\item \textit{GET\_SURROUNDING\_NODES\_LINEAR\_TRIANGULAR\_ELEMENT( )}, subroutine which is used to calculate\textit{ GET\_SURROUNDING\_NODES( )} data structure for bi-linear triangular elements.
\item \textit{GET\_SURROUNDING\_NODES\_QUADRATIC\_TRIANGULAR\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for bi-quadratic triangular elements.
\item \textit{GET\_SURROUNDING\_NODES\_CUBIC\_QUADRILATERAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for bi-cubic quadrilateral elements.
\item \textit{GET\_SURROUNDING\_NODES\_LINEAR\_HEXAHEDRAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for tri-linear hexahedral elements.
\item \textit{GET\_SURROUNDING\_NODES\_QUADRATIC\_HEXAHEDRAL\_ELEMENT( )}, subroutine which is used to calculate\textit{ GET\_SURROUNDING\_NODES( )} data structure for tri-quadratic hexahedral elements.
\item \textit{GET\_SURROUNDING\_NODES\_CUBIC\_HEXAHEDRAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for tri-cubic tetrahedral elements.
\item \textit{GET\_SURROUNDING\_NODES\_LINEAR\_TETRAHEDRAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for tri-linear hexahedral elements.
\item \textit{GET\_SURROUNDING\_NODES\_QUADRATIC\_TETRAHEDRAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for tri-quadratic tetrahedral elements.
\item\textit{ GET\_SURROUNDING\_NODES\_CUBIC\_TETRAHEDRAL\_ELEMENT( )}, subroutine which is used to calculate \textit{GET\_SURROUNDING\_NODES( )} data structure for tri-cubic tetrahedral elements.
\end{itemize}
\end{enumerate}
The above mentioned input data structures are passed to the subroutine \textit{DECOMPOSITION\_NODE\_DOMAIN\_CALCULATE( )} where \textit{ParMETIS\_V3\_PartKway( )} routine calculates the data structure \textit{DECOMPOSITION\%NODE\_DOMAIN(global\_node\_idx)}. 
After calculating the vertex domains, the next step is to use calculate the following entities. 
\begin{itemize}
\item The corresponding degree of freedom(DOF) corresponding to each vertex.
\item Internal vertices/degrees of freedom(DOFs) corresponding to each subdomain.
\item Boundary vertices/degrees of freedom(DOFs) corresponding to each subdomain.
\item Ghost vertices/degrees of freedom(DOFs) with respect to a subdomain. Here it is important to explain the concept of ghost vertices/degrees of freedom(DOFs). Multithreaded simulations typically require the subdomains to overlap i.e. the vertices or elements on each side of the boundary between subdomains will exist in more than one subdomains. The first subdomain that a vertex belongs to is the one that it is ``actually'' been assigned by graph partitioning and the other subdomains a vertex belongs to is any subdomain spatially adjacent to it. Therefore, a vertex that belongs to a particular domain which is not its actual domain is termed as a ghost vertex with respect to that particular domain.
\end{itemize}
These parameters are calculated in \textit{DOMAIN\_MAPPING\_NODES\_DOF\_CALCULATE\_NEW( }). The following logic is used for calculating each one of the above mentioned parameters.
\begin{itemize}
\item Each degree of freedom shares the same subdomain as its corresponding node. 
\item Vertices/DOFs that are surrounded by vertices/DOF of the same subdomain are calculated as internal node.
\item Vertices/DOFs that are surrounded by vertices/DOFs of a different domain are calculated as boundary vertices.
\item A vertex is termed as a ghost node with respect to a particular subdomain if the node is a boundary node with respect to its actual subdomain and its adjacent to that particular subdomain.
\end{itemize}
A schematics illustrating internal and ghost vertices in a graph are illustrated in figure $5.2$.
\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{ch5figure1.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.2 Schematic illustrating internal and ghost vertices of a graph.}
\end{figure}
Now using information related to internal, boundary and ghost vertices, the following entities are calculated in the next step.
\begin{itemize}
\item Internal element corresponding to each subdomain.
\item Boundary element corresponding to each subdomain.
\item Ghost element with respect to each subdomain.
\end{itemize}
The above mentioned entities are calculated in \textit{DOMAIN\_MAPPING\_}\textit{ELEMENT\_CALCULATE\_NEW( )}. The new modules of the vertex-based partitioning are added in subroutine \textit{DECOMPOSITION\_CREATE\_FINISH( )}. The workflow of the subroutine \textit{DECOMPOSITION\_CREATE\_FINISH( )} is illustrated in figure 5.3. 
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch5figure5.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.3 The workflow of subroutine \textit{DECOMPOSITION\_CREATE\_FINISH( )}.}
\end{figure}
\subsection{Subroutines introduced in \textit{OPENCMISS\_IRON} module}
In the third step, the subroutines that users can introduce in the \textit{FortranExample.f90} file to utilize the vertex based decomposition feature of \textit{OpenCMISS-Iron} are explained. The calling sequence of these subroutines is explained in the appendix section 3.1.2 and a brief detail along with their functionalities is explained as follows.
\begin{enumerate}
\item Subroutine \textit{CMFE\_DECOMPOSITION\_NODE\_BASED\_DECOMPOSITION\_SET( )} to activate the vertex based decomposition. This subroutine can be called to switch \textit{DECOMPOSITION\%NODE\_BASED\_DECOMPOSITION} to \textit{TRUE}.
\item Subroutine \textit{CMFE\_DECOMPOSITION\_NUMBER\_OF\_CONSTRAINTS\_SET( )} to assign define \textit{DECOMPOSITION\%NUMBER\_OF\_CONSTRAINTS} member. The default value of this member if $1$. 
\item Subroutine \textit{CMFE\_DECOMPOSITION\_VERTEX\_WEIGHTS\_SET( )} is an array of size equal to the number of local vertices with a default value of $1$. 
\item Subroutine \textit{CMFE\_DECOMPOSITION\_EDGE\_WEIGHTS\_SET( )} to initialize the data structure \textit{DECOMPOSITION\%EDGE\_WEIGHTS}. By default, \textit{DECOMPOSITION\%EDGE\_WEIGHTS} is an array of size equal to the number of local edges with a default value of $1$.
\item Subroutine \textit{CMFE\_DECOMPOSITION\_UBVEC\_SET( )} to define the data structure \textit{DECOMPOSITION\%UBVEC}. The default value of this data structure is initialized to $1.001$.
\item Subroutine \textit{CMFE\_DECOMPOSITION\_TPWGT\_SET( )} to initialize \textit{DECOMPOSITION\%TPWGTS} parameter. The default value is set to $1.0/$ \textit{DECOMPOSITION\%NUMBER\_OF\_PARTS}.
\item Subroutine \textit{CMFE\_DECOMPOSITION\_VTX\_DIS\_SET( )} to initialize the array \textit{DECOMPOSITION\%VTX\_DIST}. The default value of array \textit{DECOMPOSITION\%VTX\_DIST} is defined such that each processor gets at maximum \textit{TOTAL\_GLOBAL\_VERTICES/ DECOMPOSITION\%NUMBER\_OF\_PARTS} vertices.
\item Subroutine \textit{CMFE\_DECOMPOSITION\_WEIGHT\_FLAG\_SET( )} to set the value of \textit{DECOMPOSITION\%WEIGHT\_FLAG}. By default, the value of \textit{DECOMPOSITION\%WEIGHT\_FLAG} is set to $0$. 
\end{enumerate}
\subsection{Verification of the implementation of vertex-based decomposition}
In order to verify the correct working of trivial decomposition, a Laplace equation is solved on a hexahedral domain with a unit volume. The problem definition of the case study is as follows.
\begin{equation}
\Delta T=0
\end{equation}
\begin{equation}
T|_{\Gamma_1^D}=0
\end{equation}
\begin{equation}
T|_{\Gamma_2^D}=3
\end{equation}
The schematic of the test case is illustrated in figure $5.4$. 
\begin{figure}[H]
\centering
\includegraphics[width=0.5\textwidth]{ch5figure3.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.4 A schematic of the Laplace study to be executed using vertex-based decomposition feature of \textit{OpenCMISS-Iron}.}
\end{figure}
The hexahedral domain is meshed with $6$x$6$x$6$ elements. The simulation is executed using $6$ processors and the results are displayed in figure $5.5$. 
\begin{figure}[H]
\centering
\includegraphics[width=0.8\textwidth]{ch5figure4.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.5 Results from the Laplace study executed in concurrently with 6 processors.}
\end{figure}
The result shows an expected trend where a linear change in temperature is observed from $z=0$ to $z=1$. This results verifies the correct implementation of the vertex-based decomposition functionality in OpenCMISS-Iron.
\section{Incorporating coupling aware partitioning algorithm into OpenCMISS-Iron} 
After adding vertex-based decomposition functionality in OpenCMISS-Iron, the next step is to move the coupling aware partitioning algorithm to OpenCMISS-Iron by following steps illustrated in figure $5.1$. The scheme behind the proposed partitioning algorithm has already been explained in section $4.3.3$. Here, the data structures and the subroutine involved in implementing each step of the algorithm will be explained in detail.  
\subsection{Derived data type members introduced in \textit{TYPES} module}
In order to facilitate implementing the partitioning algorithm in OpenCMISS-Iron, a derived data type \textit{COUPLED\_DECOMPOSITION\_TYPE} is defined in \textit{TYPES} module. The members of the derived data type for a \textit{COUPLED\_DECOMPOSITION\_TYPE} object are as follows.
\begin{itemize}
\item \textit{COUPLED\_DECOMPOSITION\%USER\_NUMBER} is a pointer of type \textit{INTEGER} used as an identifier for the \textit{COUPLED\_DECOMPOSITION\_TYPE} derived type objects.
\item \textit{COUPLED\_DECOMPOSITION\%mesh\_idx} is a member of type \textit{INTEGER} used as an index for coupled mesh and the interface graphs. Indices $COUPLED\_DECOMPOSITION\%mesh\_idx=1$ and $COUPLED\_DECOMPOSITION\%mesh\_idx=2$ are used for the coupled mesh graphs. Whereas, $COUPLED\_DECOMPOSITION\%mesh\_idx=3$ is used for the interface graph. 
\item \textit{COUPLED\_DECOMPOSITION}\textit{\%COUPLED\_FIELD}(:) is any array of \textit{FIELD\_TYPE} objects representing geometric fields of the coupled mesh graph $G_i$ and the interface graph $G_I$. \textit{COUPLED\_DECOMPOSITION\%COUPLED\_FIELD(COUPLED\_DECOMPOSITION} \textit{\%mesh\_idx}), where $COUPLED\_DECOMPOSITION\%mesh\_idx=1$ and $COUPLED\_DECOMPOSITION\%mesh\_idx=2$ represents geometric field objects of the coupled mesh graphs $G_A$ and $G_B$, whereas $COUPLED\_DECOMPOSITION\%mesh\_idx=3$ represents the geometric field object of the interface graph $G_I$.
\item \textit{COUPLED\_DECOMPOSITION}\textit{\%INTER\_EDGES}(:,:) is a $2$D array representing a binary relationship between the vertices of the coupled mesh graph $G^I_i$ and the interface graphs $G_I$. Each row \textit{COUPLED\_DECOMPOSITION}\textit{\%INTER\_EDGES(inter\_edge\_idx},:) represents vertices on sides of the interedge $I_{iI}$ represented by \textit{inter\_edge\_idx}. 
\item \textit{COUPLED\_DECOMPOSITION\%}\textit{OLD\_TO\_NEW\_VERTEX\_MAPPING}(: , :) is a $2$D array representing one-to-one mapping between the vertices of the original coupled graph $G_i$ and the new coupled mesh graph $G_{i,merged}$, formed as a result of merging vertices of the original coupled mesh graph. 
\item \textit{COUPLED\_DECOMPOSITION}\textit{\%COUPLED\_MESH\_COORDINATES}(: , :) is a $2$D array that stores the geometric field of a coupled mesh graph $G_i$. 
\item \textit{COUPLED\_DECOMPOSITION}\textit{\%INTERFACE\_MESH\_COORDINATES}(: , :) is a $2$D array that stores the geometric field of an interface mesh graph $G_I$. 
\end{itemize}
\subsection{Subroutines introduced in \textit{MESH\_ROUTINES} module}
In the second step, a set of routines are introduced for purpose of implementing the partitioning algorithm. The calling sequence of these subroutines is explained in the appendix section 3.2.1 and a brief detail along with their functionalities are explained as follows.
\subsubsection{\textit{SUBROUTINE COUPLED\_DECOMPOSITION\_CREATE\_START(  )}}
In this subroutine, the members of \textit{COUPLED\_DECOMPOSITION}\textit{\_TYPE} objects are set to their default values. 
\subsubsection{\textit{SUBROUTINE COUPLED\_DECOMPOSITION\_ADD\_COUPLED\_MESH ( )}}
This subroutine is used to store objects of \textit{FIELD\_TYPE} in the array \textit{COUPLED\_DECOMPOSITION\%COUPLED\_FIELDS(:)}. The \textit{FIELD\_TYPE} objects represent the geometric field of a coupled mesh graph $G_i$. This subroutine is called twice, to store geometric fields of the coupled mesh graphs $G_A$ and $G_B$. 
\subsubsection{\textit{SUBROUTINE COUPLED\_DECOMPOSITION\_ADD\_INTERFACE\_MESH( )}}
This subroutine is used to store objects of \textit{FIELD\_TYPE} in the \textit{COUPLED\_DECOMPOSITION\%COUPLED\_FIELDS(3)}. The \textit{FIELD\_TYPE} object represent geometric field of an interface mesh graph $G_I$.
\subsubsection{\textit{SUBROUTINE COUPLED\_DECOMPOSITION\_CREATE\_FINISH ( )}}
\textit{SUBROUTINE COUPLED\_DECOMPOSITION\_CREATE\_FINISH( )} is the subroutine where the scheme of partitioning algorithm described in section $4.3.3$ is implemented. In the following, subroutines used to implement each step of the partitioning algorithm will be discussed in more detail. 
\begin{enumerate}
\item Before start implementing the partitioning scheme, the first step is to gather geometric fields corresponding to the coupled mesh graphs $G_A$ and $G_B$ and the interface graph $G_I$ and store them in data structures \textit{COUPLED\_DECOMPOSITION\%COUPLED\_MESH\_COORDINATES} and \textit{COUPLED\_DECOMPOSITION\%INTERFACE\_COORDINATES} respectively using \textit{MPI\_ALLREDUCE( )} routine. This operation is performed in subroutine \textit{COUPLED\_DECOMPOSITION\_GATHER\_GRAPH\_COORDINATES( )}. 
\item In step 2, the interface graph $G_I$ undergoes a trivial \textit{K}-way partitioning i.e. $Part (G_I,K) \rightarrow P_I$. This takes place in the subroutine \textit{ COUPLED\_DECOMPOSITION\_INTERFACE\_MESH\_TRIVIAL\_DECOMPOSITION\_SET( )}, where the subroutines described in section $5.2$ are called to perform this operation. 
\item The next step is to isolate the vertices of the coupled mesh graph $G_i$, where \textit{i}= \textit{A} or \textit{B} on the interface front i.e. $Rest(G_A,G_I) \rightarrow G_A^I $ and to build the interedges $I_{iA}$ between the vertices of the coupled mesh graph $G_i$ and the interface graph $G_I$ i.e. $Proj(v_I) \rightarrow v_A^I$. These operations are carried out in the subroutine \textit{COUPLED\_DECOMPOSITION\_ INTER\_EDGES\_SET (  )}. 
\item From here onward, the process of fixed vertex partitioning is initiated i.e. $FixPart(G_i(V,E, W^e, W^v),P(G_A^I)) \rightarrow P_i$. For initiating fixed vertex partitioning, users can choose between scheme A and scheme B. The set of subroutines called to implement each scheme are as follows. 
\begin{itemize}
\item Scheme A 
\newline
Recall that in section $4.2$, fixed vertex partitioning is imposed by assigning large edge weights to edges with similar subdomains on the ends and small vertex weight on edges with dissimilar subdomains on the ends. Moreover, vertex weight on the vertices of interface fronts of the coupled mesh i.e. on the subgraph $G^I_i$, is assigned such that combined vertex weights on a set of vertices belonging to each subdomains is assigned a value equal to the number of vertices of the coupled graph $G_i$. Therefore in the first step, vertex weights are assigned to vertices $v_i^I \in V_i^I$ of the subgraph $G_i^I(V_i^I,E_i^I)$. This task is performed in the subroutine \textit{COUPLED\_DECOMPOSITION\_VERTEX\_WEIGHTS\_SCHEME\_A\_SET(  )}. In the step $2$, the edge weights are assigned on edges $e_i^I \in E_i^I$ of the subgraph $G_i^I(V_i^I,E_i^I)$ in subroutine \textit{COUPLED\_DECOMPOSITION\_EDGE\_WEIGHTS\_SCHEME\_A\_SET(  )}. In step $3$, a \textit{k}-way partitioning of the coupled mesh graph $G_i(V_i,E_i)$ is executed using \textit{ParMETIS\_V3\_PartKway} routine. In the final step, the local vertex domains from each computational node are gathered in one data structure \textit{DECOMPOSITION\%NODE\_DOMAIN(  )} using \textit{MPI\_SEND(  )} and \textit{MPI\_RECV(  )} subroutines.  
\item Scheme B 
\newline
Recall scheme B in section $4.2$, where fixed vertex partitioning is imposed by merging vertices with common subdomain and assigning large weights to them to make sure each one of the merged vertices belongs to a different subdomain. Therefore, the step $1$ of the scheme B is to group the vertices of the subgraph $G_i^I(V_i^I,E_i^I)$ together which are supposed to have an identical subdomain. This operation is carried out in subroutine \textit{COUPLED\_DECOMPOSITION\_VERTICES\_TO\_MERGE( )} and \textit{COUPLED\_DECOMPOSITION\_GET\_NEW\_GRAPH( )} . In step $2$, the a new graph is generated using information from the step $1$. The new graph $G_{i,merged}(V_i,E_i)$ contains the merged vertices in the subgraph $G_{i,merged}^I(V_{i,merged}^I,E_{i,merged}^I)$ and a new vertex numbering. One of the important output data structure is a $2$D array \textit{COUPLED\_MESH\_NEW\_TO\_OLD\_NODE\_MAPPING (: , :)} which contains a one-to-one mapping between the vertex ids of old and the new coupled mesh graphs. In step $3$, corresponding to the first constraint, the merged vertices are assigned a vertex weight of magnitude equal to the number of vertices in a graph. Whereas corresponding to the second constraint, the merged vertices are assigned a weight equal to the number of merged vertices, that each vertex of the new graph represents. This operation is carried out in subroutine \textit{COUPLED\_DECOMPOSITION\_VERTEX\_WEIGHTS\_SCHEME\_B\_SET( )}. In step $4$, a \textit{K}-way partitioning of the coupled mesh graph $G_{i,merged}(V_i,E_i)$ is executed using \textit{ParMETIS\_V3\_PartKway} routine. In step $5$, the local vertex domains from each computational node are gathered in one data structure \textit{DECOMPOSITION\%NODE\_DOMAIN ( : )} using \textit{MPI\_SEND( )} and \textit{MPI\_RECV( )} subroutines. In the final step, the vertex domains of the new coupled mesh graph $G_{i,merged}(V_i,E_i)$ are projected on the old coupled mesh graph $G_i(V_i,E_i)$  using information contained in the data structure \textit{COUPLED\_MESH\_NEW\_TO\_OLD\_VERTEX\_MAPPING(: , :)}. Figure 5.5 illustrates the workflow for scheme B using 2D schematics of a coupled mesh model.
\end{itemize}
\end{enumerate}
The above mentioned subroutines are executed in the order illustrated in figure $5.6$. 
\begin{figure}[H]
\centering
\includegraphics[width=1.1\textwidth]{ch5step3.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.6 Workflow of the partitioning algorithm based on scheme B: In step $1$, the interface graph $G_I$ is trivially decomposed. In step $2$, interedges $I_{iI}$ are built across which identical subdomains are to be imposed. In step $3$, a mapping is established between the vertices of the coupled mesh graph $G_i$ and the new merged coupled mesh graph $G_{i,merged}$. }
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=1.1\textwidth]{ch5step2.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.6 Workflow of the partitioning algorithm based on scheme B (continued): In step $4$, the new merged coupled mesh graph $G_{i,merged}$ is formed. In step $5$, weights are assigned to the merged vertices to make sure ParMETIS assigns a unique subdomain to each merged vertex. In step $6$, the merged coupled mesh graph $G_{i,merged}$ is partitioned.  }
\end{figure}
\begin{figure}[H]
\centering
\includegraphics[width=1.1\textwidth]{ch5step1.png}
\captionsetup{labelformat=empty}
\caption{Figure 5.6 Workflow of the partitioning algorithm based on scheme B (continued): In the final step, the vetex partitioning of the merged coupled mesh graph $G_{i,merged}$ is projected on to the original coupled mesh graph $G_{i}$.  }
\end{figure}
\subsubsection{Subroutine incorporated into \textit{OPENCMISS\_IRON} module}
The subroutines that are incorporated into \textit{OPENCMISS\_IRON} routine can be used by user to activate partitioning algorithm in an interface multiphysics study in \textit{FortranExample.f90} file. Inside these subroutine, the routines introduced in \textit{MESH\_ROUTINES} module are called. Therefore the routines introduced in \textit{OPENCMISS\_IRON} module acts as a bridge between the routines in \textit{MESH\_ROUTINES} module where partitioning algorithm is implemented and the \textit{FortranExample.f90} file. The calling sequence of these subroutines is explained in the appendix section 3.2.2 and a brief detail along with their functionalities is explained as follows.
\begin{itemize}
\item \textit{CMFE\_Coupled\_Decomposition\_Initialize()} routine where the \textit{CMFE\_COUPLED\_DECOMPOSITION\_TYPE} object is nullified. 
\item \textit{CMFE\_Coupled\_Decomposition\_CreateStart()} routine where \textit{COUPLED\_DECOMPOSITION\_CREATE\_START()} routine is called. 
\item \textit{CMFE\_Coupled\_Decomposition\_AddCoupledMesh()} routine where \textit{COUPLED\_DECOMPOSITION\_ADD\_COUPLED\_MESH()} routine is called.
\item \textit{CMFE\_Coupled\_Decomposition\_AddInterfaceMesh()} routine where \textit{COUPLED\_DECOMPOSITION\_ADD\_INTERAFACE\_MESH()} routine is called.
\item \textit{CMFE\_Coupled\_Decomposition\_CreateFinish()} routine where \textit{COUPLED\_DECOMPOSITION\_CREATE\_FINISH()} is called. 
\end{itemize}
\section{Incorporating partitioning algorithm in \textit{GenericFortranExample.f90}}
After incorporating the partitioning algorithm in OpenCMISS-Iron, the next step is to be able to activate the partitioning algorithm related subroutines via an input file. For this purpose, a block of subroutines is defined in the \textit{GenericFortranExample.f90} file. The block consist of subroutines explained in $5.3.3$. This block is activated using the following lines provided in the input file. 
\newline
\newline
\fbox{\begin{minipage}{40em}
$\mathit{START\_COUPLED\_DECOMPOSITION}$
\newline
\newline
\newline
$\mathit{COUPLED\_DECOMPOSITION\_ID}$
\newline
$\mathit{COUPLED\_DECOMPOSITION\_}1$
\newline
\newline
$\mathit{COUPLED\_MESH\_TO\_ADD}$ ! \textit{Id of coupled mesh graphs} $\mathit{G_A}$ \textit{and} $\mathit{G_B}$.
\newline 
\textit{FLUID, SOLID}
\newline
\newline
$\mathit{INTERFACE\_MESH\_TO\_ADD}$  ! \textit{Id of interface mesh graphs} $\mathit{G_I}$
\newline 
\textit{INTERFACE}
\newline
\newline
$\mathit{END\_COUPLED\_DECOMPOSITION}$
\end{minipage}}
\newline
\section{Summary}
In this chapter, an overview of the subroutines introduced to integrate vertex-based decomposition and partitioning algorithm in OpenCMISS-Iron is presented. Moreover, the functionalities introduced in the parsing algorithm to activate the parsing algorithm is also explained. Some of the important take away from that chapter are as follows. 
\begin{enumerate}
\item The vertex-based decomposition functionality introduced in robust and similar to element-based decomposition, can be used to solve any case study. The robustness of vertex-based simulation is established by solving a Laplace problem with $6 \times 6 \times 6$ elements using $6$ processors. 
\item The partitioning algorithm introduced in \textit{MESH\_ROUTINES} module can be used for the following element types.
\begin{itemize}
\item Line elements with linear, quadratic and cubic interpolation order.
\item Quadrilateral elements with bi-linear, bi-quadratic and bi-cubic interpolation order.
\item Triangular elements with bi-linear, bi-quadratic and bi-cubic interpolation order.
\item Hexahedral elements with tri-linear, tri-quadratic and tri-cubic interpolation order.
\item Tetrahedral elements with tri-linear, tri-quadratic and tri-cubic interpolation order.
\end{itemize}
\item Users can activate a the partitioning algorithm in a generic interface-coupled multiphysics study using input file user interface.
\end{enumerate}
In the next chapter, a number of computational test cases will be presented to establish the robustness of the partitioning algorithm and its comparison with state of the art OpenCMISS-Iron. 