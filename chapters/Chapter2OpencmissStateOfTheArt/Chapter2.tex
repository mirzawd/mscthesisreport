%--------------------------------------------------------------------------------------------------
%
\chapter{OpenCMISS-Iron: State of the art}
The purpose of this chapter is to explain the state of the art architecture and features of OpenCMISS-Iron with emphasis on the following.
\begin{enumerate}
  \item Multiphysics and multiscale functionalities of the state of the art source code of OpenCMISS-Iron library. 
  \item Input file user interface functionalities of OpenCMISS-Iron, which involves a parsing algorithm capable of setting up a generic simulation based on a set of instructions provided to the source code via an input file.
  \item Introduction of ParMETIS based mesh partitioning functionalities of OpenCMISS-Iron.
\end{enumerate}
The structure of the chapter is as follows: Section 2.1 gives a brief but a comprehensive overview of the OpenCMISS-Iron software, its design goals and multiphysics capabilities. Section 2.2 sheds light on the algorithm behind the input file user interface functionalities of generic parsing algorithm. Section 2.3 introduces some terminologies from the graph theory and overviews the MPI-based third-party graph partitioning library called ParMETIS. Finally, section 2.4 explains state of the art mesh decomposition functionalities of OpenCMISS-Iron.
\section{OpenCMISS-Iron: Introduction and design goals}
OpenCMISS-Iron is an open source, multiphysics computational modelling software
developed as part of the VPH/Physiome project \cite{Brad}. OpenCMISS-Iron is capable of solving
multiple sets of coupled differential equations (such as finite elasticity equation coupled with fluid mechanics equation) and linking different spatial scales
(such as linking sub-cellular and tissue level biophysical processes to phenomena manifesting at organ level). Moreover, OpenCMISS-Iron is developed for distributed memory architecture and can take advantage of modern programming languages, data structures, and high-performance computing hardware. The features of OpenCMISS-Iron that set it apart from the other commercial softwares are the design goals that OpenCMISS-Iron is developed on. Some of the design goals are the following \cite{Brad}:
\begin{enumerate}
 \item The OpenCMISS-Iron libraries are developed such that they are extensible,
 flexible and customizable to unforeseen coupled problems that may arise from future applications. 
This is made possible by using generic data structures that are capable of storing the data 
for diverse modelling problems and are expressed in a common structure, 
which allows a flexible coupling between different partial differential equations.
 \item The second design goal is that OpenCMISS-Iron should be inherently
 adaptable to parallel environment and hardware with diverse parallel architectures. OpenCMISS-Iron meets this goal by using a general \textit{n}$\times$\textit{p}(\textit{n})$\times$\textit{e}(\textit{p}) hierarchical
 parallel environment where \textit{n} is the number of computational nodes,
 \textit{ p}(\textit{n}) is the number of processing systems on the computational node and
 \textit{e}(\textit{p}) is the number of processing elements for each
 processing system.
  For example, this hierarchy is capable of taking the following forms.
 	\begin{itemize}
 	 \item Multi-core \textit{n}$=1$,
 	 \textit{p}(\textit{n})$>1$, \textit{e}(\textit{p})=$1$
 	 \item Pure cluster \textit{n}$>1$,
 	  \textit{p}(\textit{n})$=1$, \textit{e}(\textit{p}) = $1$
 	 \item Multi-core cluster
 	 \textit{n}$\geq 1$, \textit{p}(\textit{n})$>1$, \textit{e}(\textit{p})=$1$ 
 	 \item Multi-core cluster with GPUs \textit{n}$>1$,
 	  \textit{p}(\textit{n})$>1$, \textit{e}(\textit{p})$>1$
 	\end{itemize}
 	\item The last design goal is that OpenCMISS-Iron should be able to be used and
 	developed both by experts and less experienced users. This goal has been achieved by,
 	\begin{itemize}
 	 \item By encapsulating complex model details in hierarchal data
 	 structures. For beginners, such hierarchal structure encapsulates and hides complex details that are set by programmers to default parameters and at the same time the object interface allows experts to manipulate the information, if required. 
 	 \item By introducing an add-on parsing algorithm which lets users set
 	 up a generic simulation in OpenCMISS-Iron by providing simulation parameters using an input file. Section 2.2 sheds more light on this functionality.
 	\end{itemize}
\end{enumerate}
\subsection{Multiphysics modelling}
A major feature of OpenCMISS-Iron includes a flexible and reusable architecture to deal with multiple physical models. The flexible and reusable architecture involves different objects for storing equations of different physics. For example in a coupled system of fluid and solid mechanics, the equations for fluid and solid parts would be stored in separate objects. Each set of physical equations can then be considered and constructed independently and fed to the solver dependencies.
In physical terms, coupling can occur within the same region (for instance, when parabolic differential equation of momentum representing finite elasticity is coupled with a parabolic partial differential equation representing temperature field) or between different regions (for instance, in fluid-solid interaction (FSI) analysis where parabolic partial differential equation of momentum representing finite elasticity is coupled with the Navier Stokes equation representing fluid flow). In OpenCMISS-Iron, a coupling between different regions can be accomplished using a number of different methods to relate the equations. These methods impose interface conditions between the fields in each region. In OpenCMISS-Iron, they are termed interface conditions. The interface conditions can be derived robustly from the weak forms of the governing equations of the coupled domains. The interface conditions are either imposed in a strong sense (at each node of the interface) or a weak sense (in an integral sense over a surface). Interface conditions can be strongly imposed using row and column manipulation of the resulting matrix from the system of equations. On the other hand, for the weak imposition of interface conditions, OpenCMISS-Iron employs Lagrange, augmented Lagrange and Penalty methods. Interface conditions using weak form are imposed not at each node of the interface rather over the interface surface in an average sense \cite{Nords}. 
\section{Input file user interface}
The aim of the input file user interface is to provide users with a functionality to
simulate a generic simulation in OpenCMISS-Iron using input parameters provided by the user. This parsing algorithm was developed in the preliminary part of the current project\cite{Mirza}. In a nutshell, the parsing algorithm consists of three layers.
\begin{enumerate}
 \item The first layer consists of input file made in coherence with the structure
 of the source code of OpenCMISS-Iron. Users can provide simulation parameters in a
 specific format outlined in \cite{Mirza}.
 \item A parsing algorithm serves as the second layer that reads the set of
 instructions from the input file and feeds them to
 the \textit{GenericFortranExample.f90}.
 \item Third layers consist of a \textit{GenericFortranExample.f90} file where a generic simulation is set up using OpenCMISS-Iron subroutines. 
\end{enumerate}
A layout of the input file user interface is illustrated in figure 2.1. Another
major advantage of the input file user interface is that it allows users to run a parametric analysis without having to 
 recompile the source files of the case study every time upon changing the simulation parameters.\\
\newline
The input file user interface has been tested for problems involving Laplace
equations (temperature problems), Cauchy equation of momentum (finite deformation problems) and Navier Stokes equations (fluid flow problem). As of now, the input file user interface is only capable of setting up single domain and single physics simulations. Nevertheless, the algorithm of the input file user interface is quite flexible and extensible to a diverse set of differential equations (in other words for any end-user applications). In the first part of the current project, the algorithm of the input file user interface is extended to solving a multi-domain Laplace problem and multiphysics poroelasticity problem (Chapter 3) where the input file user interface will be discussed more in detail.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch2figure1.png}
\captionsetup{labelformat=empty}
\caption{Figure 2.1 Generic workflow of the add-on parsing algorithm.}
\end{figure}
\section{Mesh partitioning for parallel applications}
\subsection{Introduction}
Mesh partitioning is a key part of preprocessing in many high-performance computing simulations especially in distributed architectures where an efficient partitioning can greatly reduce the computational cost and execution time of an analysis. Mesh partitioning is a static load balancing of mesh that involves dividing mesh among several processors in a distributed environment such that the following goals are met.
\begin{enumerate}
\item Each partition has the same number of vertex weights. Vertex weights in a partition is a measure of the computational cost involved to process a partition.
\item The number of edgecuts i.e. edges belonging to more than one partition are minimised. Number of edgecuts resulting from partitioning are proportional to the interprocessor communication cost.
\end{enumerate}
More formally, consider a graphical representation of a mesh \textit{G=(V,E)}, where \textit{V} is the set of vertices or nodes and \textit{E} is the set of edges of the mesh. Each vertex $\mathit{v \in V}$ is assigned a weight \textit{w(v)} representing its computational cost. While the edges $\mathit{e \in E}$ is assigned a weight \textit{w(e)} representing the communication cost between the vertices on either side of the edge. Suppose a graph \textit{G=(V,E)} is divided among \textit{K} processors such that in graph partitioning, the vertices assigned to \textit{K} processors are $\mathit{(V_{1},V_{2},.....,V_{K})}$, where $\mathit{V_{i} \cap V_{j} = \emptyset}$ for all $\mathit{1 \leq i}$, $\mathit{j \leq K}$, $\mathit{i\neq j}$ and $\mathit{V_{1} \cup V_{2} \cup, .....,\cup V_{K} = V}$. A balanced partitioning according to the goal $1$ is achieved when work assigned to each processor \textit{W(k)}, for \textit{k = $1$..K}, is such that:
\begin{equation}
W_{k} \leq W_{avg}(1+\epsilon)
\end{equation}
Where $\epsilon$ is the imbalance tolerance with an optimum value of 5\%. Moreover, in equation 2.1, $\mathit{W_{k}}$ and $\mathit{W_{avg}}$ are calculated using the following equation.
\begin{equation}
W_{k} = \sum\limits_{i=m}^n w(v_{i}), for \hspace{3pt} {v_{m} \cup ... \cup v_{n}}=V_{k} 
\end{equation}
\begin{equation}
W_{avg} = \frac{\sum \limits_{i=1}^K W_{k}}{K}
\end{equation}
Furthermore, a mathematical definition of total edgecut cost $C_e$ resulting from mesh partition is presented as follows:
\begin{equation}
C_{e} = \sum_{e \in F} w(e)
\end{equation}
where $ F = \left \{(a,b) \in E, a \in V_{i} , b \in V_{j} , V_{i} \neq V_{j} \right \}$. Goal $2$ of the partitioning is to minimise the communication volume, presented by equation $2.4$, between the partitions.
\subsection{State of the art partitioning algorithms of OpenCMISS}
OpenCMISS-Iron utilizes a MPI-based parallel library called ParMETIS for partitioning of structured and unstructured mesh. The subroutines of ParMETIS are based on the multilevel partitioning and fill ordering algorithms \cite{George}. In general, ParMETIS provides functionalities such as.
\begin{enumerate}
\item Partitioning and distribution of unstructured/structured mesh among multiple processors such that each processor gets the same workload.
\item Repartitioning adaptive meshes.
\item Repartitioning in order to improve the quality of existing partitioning.
\item Computing fill-reducing orderings for sparse matrices that reduce the computation cost of direct factorization schemes. 
\end{enumerate}
ParMETIS achieves mesh partitioning using a multilevel graph partitioning that works by applying partitioning in one or more stages. Each stage reduces the size of the graph by collapsing vertices and edges, partitions the smaller graph, then maps back and refines this partition of the original graph. In other words, the methods consist of 3 phases which are as follows.
\begin{enumerate}
\item The coarsening phases: During this phase, the graph size is consecutively decreased. 
\item The initial partitioning phase: In this phase, a \textit{k}-way partitioning of the graph is done.
\item The uncoarsening phase: In this phase, the partitioning is consecutively refined as it is projected to a larger graph. The size of the largest partitioned graph would be the size of graph provided by the user. 
\end{enumerate}
An illustration of this concept is depicted in figure 2.2 \cite{George}.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch2figure2.png}
\captionsetup{labelformat=empty}
\caption{Figure 2.2 Three phases of multi \textit{k}-way partitioning approach i.e. 1) The coarsening phase where the size of the graph is decreased. 2) Initial partitioning phase where the graph is partitioned in \textit{k} sub-domains. 3) The Uncoarsening phase where partitioning is refined to bring graph back to initial size.}
\end{figure}
\subsection{Partitioning routines of ParMETIS}
This section gives an insight into the names and functionalities of ParMETIS subroutines. One of the reasons ParMETIS is flexible to different mesh topologies is that its algorithm is based on several subroutines with each subroutine capable of performing a unique task. A summary of the name and functionalities of different subroutines is illustrated in figure 2.3 \cite{George}. A brief detail on the ParMETIS subroutines which are relevant in context is as follows.
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch2figure3.png}
\captionsetup{labelformat=empty}
\caption{Figure 2.3 An overview of subroutines employed in algorithm of ParMETIS.}
\end{figure}
\textbf{\textit{ParMETIS\_V3\_PartKway}} is capable of computing a \textit{k}-way partitioning of a graph while minimizing the edgecuts. \textit{ParMETIS\_V3\_PartKway} effectively partitions a graph irrespective of the quality of initial distribution. If the graph is initially well distributed among the processors, \textit{ParMETIS\_V3\_PartKway} will take less time to execute. However, there will be no change in the quality of the computed partitionings for both cases. This subroutine employs multilevel partitioning as described in detail in section 2.3.2. This subroutine utilizes graph information such as vertex adjacency to compute partitioning.
\newline
\newline
\textbf{\textit{ParMETIS\_V3\_PartGeomKway}} computes partitioning of finite element based graphs using geometric information of the vertices, in addition to graph information. This routine does a low-quality initial partitioning, distributes the mesh among processors and then calls \\\textit{ParMETIS\_V3\_PartKway} to compute the final high-quality partitioning. \textit{ParMETIS\_V3\_Part}\textit{GeomKway} can be much faster than \textit{ParMETIS\_V3\_PartKway} because it employs space-filling curve method to calculate a good quality initial partitioning and as explained before, the higher the initial partitioning quality, the faster \textit{ParMETIS\_V3\_PartGeomKway} executes partitioning. 
\newline
\newline
\textbf{\textit{ParMETIS\_V3\_PartGeom}} provides low quality partitioning for unstructured/structured mesh based graphs. Although this routine is 5\textendash10 times faster \cite{George} than \textit{ParMETIS\_V3\_PartGeomKway}. The input arguments of this subroutine also contain the graph information.
\newline
\newline
\textbf{\textit{ParMETIS\_V3\_PartMeshKway}} take mesh parameters (such as element connectivity etc.) as input and assigns a partition to each mesh element. The routine converts mesh information to graph information internally and then utilizes \textit{ParMETIS\_V3\_PartMeshKway} to execute partitioning.
\newline
\newline
\textbf{\textit{ParMETIS\_V3\_RefineKway}} is the routine that can be called to improve the quality of an existing partitioning. Once a graph is partitioned, this routine can be called to compute a new partitioning that further improves the quality. However, each successive call to \textit{ParMETIS\_V3\_RefineKway} will most likely produce smaller improvements in terms of quality.
\subsection{ParaMETIS Application Programming Interface (API)}
The library of subroutines implemented in ParaMETIS standalone program can be accessed using C, C$++$ and Fortran. In this section, the format of the data structures that users are supposed to provide for calling subroutines is discussed in detail with case studies. Moreover, the format of output data structures that store computed partitioning is also discussed. 
\subsection{Format of input data structures}
Users are supposed to follow a specific format for input arguments of ParMETIS subroutine as described in the following. 
\begin{enumerate}
\item \textbf{Data structures with graph information} 
\newline
Routines such as \textit{ParMETIS\_V3\_PartKway}, \textit{ParMETIS\_V3\_PartGeomKway} and \\
\textit{ ParMETIS\_V3\_PartGeom} 
takes in data structures such as \textit{xadj} of size $n+1$ and \textit{adjncy} of size $2m$, where m and n are the number of edges and vertices. These data structures store graph information in distributed compressed serial format (CSR). This format allows the vertices and edges of the graph to be distributed among the several processors. For \textit{k}-way partitioning of a graph, there will be local \textit{xadj} and \textit{adjncy} arrays. For instance, consider the computational mesh illustrated in figure 2.4 is distributed among 3 processors namely $p_0$, $p_1$ and $p_2$. The distributed CSR format, for this case is as follows.
\newline
\newline
\fbox{\begin{minipage}{30em}
Processor 0:
\newline 
\hspace{5cm} \textit{xadj}  \hspace{1.35cm} 0 2 5 8 11 13
\newline
\hspace{5cm} \textit{adjncy} \hspace{1cm} 1 5 0 2 6 1 3 7 5 2 4 8 3 9
\newline
\hspace{5cm} \textit{vtxdist} \hspace{1cm} 0 5 10 15
\newline
Processor 1:
\newline
\hspace{5cm} \textit{xadj}	\hspace{1.35cm} 0 3 7 11 15 18
\newline
\hspace{5cm} \textit{adjncy} \hspace{1cm} 0 6 10 1 5 7 11 2 6 8 12 3 7 9 13 4 8 14
\newline
\hspace{5cm} \textit{vtxdist} \hspace{1cm} 0 5 10 15
\newline
Processor 2:
\newline
\hspace{5cm} \textit{xadj} \hspace{1.35cm}	0 2 5 8 11 13
\newline
\hspace{5cm} \textit{adjncy} \hspace{1cm} 5 11 6 10 12 7 11 13 8 12 14 9 13
\newline
\hspace{5cm} \textit{vtxdist} \hspace{1cm} 0 5 10 15
\end{minipage}}
\newline
\newline
In the block above, \textit{adjncy} array stores vertex adjacency. For instance, for processor $p_0$, \textit{adjncy[1]} and \textit{adjncy[2]} represents the vertices that surround vertex labelled as 0. Similarly entries \textit{adjncy[3]}, \textit{adjncy[4]} and \textit{adjncy[5]} represents vertices that surround the vertex 1. \textit{xadj} array always start with entry 0 and each successive entry represents the index in the array \textit{adjncy} until when the number of vertices surrounding each labelled vertex ends.
Moreover, \textit{vtxdist} array indicates the range of vertices that are local to each processor. The structure is such that processor $p_i$ stores the vertices from \textit{vtxdist[i]} up to (but not including) vertex \textit{vtxdist[i+1]}. In the block above first processor $p_0$ stores vertices labelled from 0 until 4, the second processor $p_1$ stores vertices from 5 until 9 and the third processor $p_2$ stores vertices labelled from 10 until 14. Moreover, ParMETIS also provides routines such as \textit{ParMETIS\_V3\_PartKway}, \textit{ParMETIS\_V3\_PartGeomKway} and \textit{ParMETIS\_V3\_PartGeom} that use the coordinate information of the vertices to pre-distribute the graph, which speeds up the execution of the parallel \textit{k}-way partitioning. These coordinates of the vertices are passed to the subroutine using a data structure \textit{xyz} of real type. The \textit{x}, \textit{y} and \textit{z} coordinates of vertex \textit{i} are stored at locations $\textit{xyz[3i]}$, $\textit{xyz[3i+1]}$ and $\textit{xyz[3i+2]}$. If $n_i$ is the number of local vertices and\textit{d} is the dimension of the mesh, then the size of local $\textit{xyz}$ array would be $d\times n_i$.
\begin{figure}[H]
\centering
\includegraphics[width=0.7\textwidth]{ch2figure4.png}
\captionsetup{labelformat=empty}
\caption{Figure 2.4 Computational mesh for distributed CSR format.}
\end{figure}
\item \textbf{Data structures with mesh information} 
\newline
Routines such as \textit{ParMETIS\_V3\_PartMeshKway }takes in input data structures with mesh information such as \textit{elmdist}, \textit{eptr} and \textit{eind}. These arrays are respectively analogous to \textit{vtxdist}, \textit{xadj}, \textit{adjancy} arrays in terms of structure. For instance, the set of vertices that belong to element \textit{i} is stored in array \textit{eind} starting at index \textit{eptr[i]} and ending at index $\textit{eptr[i+1]}$ (in other words, \textit{eind[eptr[i]]} up to and including $\textit{eind[eptr[i+1]-1])}$. Additionally,\textit{ ParMETIS\_V3\_PartMeshKway} requires an \textit{elmwgt} array that specifies element weights (analogous to the \textit{vwgt} array).
\end{enumerate}
\subsection{Format of output data structures}
The format of the output data structures of ParMETIS subroutines is as follows. 
\begin{enumerate}
\item\textit{Part} array of size $n_i$ (where $n_i$ is the number of local vertices assigned to each processor) stores the sub-domain Ids assigned to vertices of the mesh i.e. for each vertex \textit{j}, the sub-domain Id (or the processor id) to which this vertex belongs will be written to \textit{part[j]}. For instance, for graph represented in figure 2.4, the local part arrays for each processor would result as follows. 
\newline
\newline
\fbox{\begin{minipage}{30em}
Processor 0:
\newline 
\hspace{5cm} \textit{Part}  \hspace{1.35cm} 0 0 0 0 0 
\newline
Processor 1:
\newline
\hspace{5cm} \textit{Part}	\hspace{1.35cm} 1 1 1 1 1 
\newline
Processor 2:
\newline
\hspace{5cm} \textit{Part} \hspace{1.35cm} 2 2 2 2 2 
\newline
\end{minipage}}
\newline
\newline
\item {Part} array of size $n_i$ (where $n_i$ is the number of local elements assigned to each processor). Upon successful execution of \textit{ParMETIS\_V3\_PartMeshKway} routine, for each element \textit{j}, the sub-domain number (i.e., the processor id) to which this element belongs will be written to \textit{part[j]}. For graph represented in figure 2.4 partitioned in two sub-domains, the local part arrays for each processor would result as follows. 
\newline
\newline
\fbox{\begin{minipage}{30em}
Processor 0:
\newline 
\hspace{5cm} \textit{Part}  \hspace{1.35cm} 0 0 1 1 
\newline
Processor 1:
\newline
\hspace{5cm} \textit{Part}	\hspace{1.35cm} 0 0 1 1 
\newline
\end{minipage}}
\newline
\newline

\item \textit{EdgeCuts} stores information of the number of edgecuts resulting from the mesh partitioning.
\end{enumerate}
\section{OpenCMISS-Iron state of the art: Domain decomposition and capability}
This subsection will give an overview of the mesh partitioning functionalities of OpenCMISS-Iron. OpenCMISS-Iron employs MPI standard \cite{nd2012} for distributed parallelization and the OpenMP \cite{nd2013} standard for shared memory parallelisation. For optimal domain decomposition, parallel graph partitioning package ParMETIS is employed in OpenCMISS-Iron. In state of the art source code of OpenCMISS-Iron, \textit{ParMETIS\_V3\_PartMeshKway} routine is used for element-wise mesh partitioning. \textit{ParMETIS\_V3\_PartMeshKway} routine, as discussed before, makes use of data structures containing the information such as mesh parameters, elements weights and the desired number of sub-domains. Currently, the data structures that contain the element and vertex weights are set to their default values, which implies that domain decomposition cannot be based on the fact that certain parts of the mesh undergo more computation and require more memory allocation ( such as interface elements/vertices in fluid-solid interaction simulations). Also, \textit{ParMETIS\_V3\_PartMeshKway} routine cannot be used to assign weights to vertices or edges. Assigning edge-weight is a key step in minimizing the communication cost between the sub-domain. This is especially important in the context of interface-coupled studies where because of high communication volume at the interface, it is always optimal to partition the coupled mesh perpendicular to the interface rather along the interface. This concept is illustrated in figure 2.5. With ParMETIS subroutines such as \textit{ParMETIS\_V3\_PartKway}, users can have an option to assign weight to both vertices and edges. Hence in the mesh decomposition module \textit{MESH\_ROUTINES.f90} of OpenCMISS-Iron, there is a need to implement the following features.
\begin{enumerate}
\item Users should be able to carry out both element-wise partitioning using \textit{ParMETIS\_V3\_PartMeshKway} as well as vertex-wise partitioning using \textit{ParMETIS\_V3\_PartKway}.
\item When using \textit{ParMETIS\_V3\_PartMeshKway} and \textit{ParMETIS\_V3\_PartKway}, users should be able to assign weights to elements as opposed to always using the default values.
\item When using \textit{ParMETIS\_V3\_PartKway} users should be able to assign weights to vertices and edges of the mesh.
\item A "coupling-aware" mesh partitioning algorithm that intelligibly partitions coupled meshes by ensuring minimum interprocessor communication cost and equal vertex weights assigned to each processor. 
\end{enumerate}
\begin{figure}[H]
\centering
\includegraphics[width=1.0\textwidth]{ch2figure5.png}
\captionsetup{labelformat=empty}
\caption{Figure $2.5$ (a) Optimum mesh partitioning of coupled mesh domains $\mathit{G_A^I}$ and $\mathit{G_B^I}$ using $2$ processors, taking into account the communication volume. (b) Mesh partitioning of coupled mesh domains $\mathit{G_A^I}$ and $\mathit{ G_B^I}$ using $2$ processors, without taking into account the communication volume between the coupled meshes.}
\end{figure}